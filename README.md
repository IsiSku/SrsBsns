# Prešli smo na Jaspersoft Studio, noviji je pošto iReport više nema podršku

Svejedno, ostaviću ovde uputstvo za iReport.
Ja lično ne mogu da ga pokrenem.

- Isidora

## iReport config
Ako iReport ne može da nađe Javu iako je instalirana i JAVA_HOME namešten,
u ireport.conf namestiti
```conf
jdkhome="C:\Program Files\Java\jdk1.8.0_73"
```
ili koja god putanja do *JDK* je kod vas.

# Collation u kreiranoj bazi
Collation se podešava:
* desni klik na bazu
* properties
* options
* iz comboBoxa odabrati Croatian_CI_AS
* ok


# Gson konfiguracija

* https://google.github.io/gson/apidocs/com/google/gson/GsonBuilder.html
* https://google.github.io/gson/apidocs/

```java
final GsonBuilder builder = new GsonBuilder();
builder.excludeFieldsWithoutExposeAnnotation();
builder.serializeNulls();
```

Opcije

```java
new RenderJson(renderWhat, builder.create())
return new RenderJson(builder.create().toJson(...)) // povratna vrednost je Result

renderJson(Object what, Gson g) // povratna vrednost je void
```

Deserijalizacija:

```java
builder.create().fromJson(...)
```

# Plan rada:
https://drive.google.com/open?id=1Lgl9Zqmk_Pm2fdB5rolm-gTovtkkmw47Xdp36gMCsdY

- Isidora

# Secure modul i anotiranje
https://playframework.com/documentation/1.2.4/secure

- Isidora

# Frontend

https://github.com/daniel-nagy/md-data-table
http://stackoverflow.com/questions/31046033/ng-repeat-and-custom-directive-issue-in-thead

- Laki


# Akteri SQL unos
## Redosled unošenja podataka u tabele unutar paketa "Akteri" zbog zavisnosti:
1. Mesto
2. Preduzeće
3. Cenovnik
4. Jedinica Mere
5. PDV
6. Grupa proizvoda
7. Proizvod
8. Stavka cenovnika

9. Stopa PDV-a (nakon 5.)
10. Poslovna godina (nakon 2.)
11. Poslovni partner (nakon 2.)

- *ILA*