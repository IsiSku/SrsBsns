package controllers;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Paths;
import java.sql.Date;
import java.util.HashMap;
import java.util.List;

import models.akteri.Preduzece;
import models.dokumenti.Faktura;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import play.db.DB;
import play.mvc.Controller;

public class Reports extends Controller {
	
	public static void getKIFPDF() {
		
		String s = params.get("start", String.class);
		String f = params.get("finish", String.class);
		Integer sender = params.get("sender", Integer.class);
		
		Date start = Date.valueOf(s);
		Date finish = Date.valueOf(f);
		
		Preduzece p = Preduzece.findById(sender);
		if(!p.nasePreduzece) {
			forbidden("Zadati pošiljalac nije naše preduzeće.");
		}
		
		// Parameters for the report.
		HashMap<String, Object> props = new HashMap<>();
		props.put("start", start);
		props.put("finish", finish);
		props.put("salje", sender);
		
		try {
			
			String workingDir = System.getProperty("user.dir");
			workingDir = Paths.get(workingDir, "reports", "KIF.jasper").toString();
			
			JasperPrint jp = JasperFillManager.fillReport(workingDir, props, DB.getConnection());
			
			File pdf = File.createTempFile("izvod", ".pdf");
			JasperExportManager.exportReportToPdfStream(jp, new FileOutputStream(pdf));
			
			renderBinary(pdf, "izvod.pdf");
		} catch (Exception ex) {
			ex.printStackTrace();
			error("nope");
		}
	}
	
	/**
	 * Makes and returns the report.
	 * Not to be used from the API.
	 * @param asPdf - true if as PDF, false if as XML
	 */
	private static void makeFaktura(boolean asPdf) {
		Integer id = params.get("faktura", Integer.class);
		Faktura f = Faktura.findById(id);
		
		System.out.println(f.smer);
		if(!f.smer.equals("I")) {
			forbidden("Izabrana faktura nije izlazna.");
		}
		
		Preduzece salje = f.odKoga;
		Preduzece prima = f.zaKoga;
		
		// Parameters for the report.
		// A whole lot of values is here because there would be too much joins in the query.
		// Query is for the table.
		HashMap<String, Object> props = new HashMap<>();
		props.put("faktid", id);
		
		props.put("salje_naziv", salje.naziv);
		props.put("salje_PIB", salje.PIB);
		props.put("salje_maticniBroj", salje.maticniBroj);
		props.put("salje_ulica", salje.ulica);
		props.put("salje_broj", salje.brojUlice);
		props.put("salje_mesto", salje.mesto.naziv);
		props.put("salje_pb", salje.mesto.postanskiBroj);
		props.put("salje_email", salje.email);
		
		props.put("faktura_datum", f.datum);
		props.put("faktura_valuta", f.datumValute);
		
		props.put("prima_naziv", prima.naziv);
		props.put("prima_PIB", prima.PIB);
		props.put("prima_maticniBroj", prima.maticniBroj);
		props.put("prima_ulica", prima.ulica);
		props.put("prima_broj", prima.brojUlice);
		props.put("prima_mesto", prima.mesto.naziv);
		props.put("prima_pb", prima.mesto.postanskiBroj);
		props.put("prima_email", prima.email);
		
		try {
			
			String workingDir = System.getProperty("user.dir");
			workingDir = Paths.get(workingDir, "reports", "Faktura.jasper").toString();
			
			JasperPrint jp = JasperFillManager.fillReport(workingDir, props, DB.getConnection());
			
			if(asPdf) {
				File pdf = File.createTempFile("faktura", ".pdf");
				JasperExportManager.exportReportToPdfStream(jp, new FileOutputStream(pdf));
				renderBinary(pdf, "faktura.pdf");
			}
			else {
				File xml = File.createTempFile("faktura", ".xml");
				JasperExportManager.exportReportToXmlStream(jp, new FileOutputStream(xml));
				renderBinary(xml, "faktura.xml");
			}
			
		} catch (Exception ex) {
			ex.printStackTrace();
			error("nope");
		}
	}
	
	public static void getFakturaXML() {
		makeFaktura(false);
	}
	
	public static void getFakturaPDF() {
		makeFaktura(true);
	}

}
