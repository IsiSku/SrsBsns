package controllers;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import models.akteri.PoslovnaGodina;
import models.akteri.Preduzece;

/**
 * 
 * @author Boki
 *
 */

public class Narudzbenica extends GenericController {

	/**
	 * Ucitava sve narudzbenice
	 */
	public static void getAll() {		
		List<models.dokumenti.Narudzbenica> narudzbenice = models.dokumenti.Narudzbenica.findAll();

		List<Object> data = prepareData(narudzbenice);
		
		GsonBuilder builder = new GsonBuilder();
		builder.excludeFieldsWithoutExposeAnnotation();
		builder.serializeNulls();
		Gson gs = builder.create();
		
		renderJSON(data, gs);
		
	}
	
	public static void kreirajFakturu(int id){
		System.out.println("Kreiranje fakture na osnovu narudzbenice");
	
		Gson g = new Gson();
		
		models.dokumenti.Narudzbenica n = models.dokumenti.Narudzbenica.findById(id); 
				//g.fromJson(body, models.dokumenti.Narudzbenica.class);
		
		models.dokumenti.Faktura f= new models.dokumenti.Faktura();
		
		List<models.dokumenti.Faktura> fakture = models.dokumenti.Faktura.find("bySmerLike","%i%").fetch();
		
//		int brIzlaznih = 0;
//		for(models.dokumenti.Faktura faktura: fakture){
//			if(faktura.smer.toLowerCase().equals("i")){
//				brIzlaznih++;
//			}
//			
//		}
//		List<StavkaCenovnika> sc = n.odKoga.cenovnikList.get(n.odKoga.cenovnikList.size()-1).stavkeCenovnika;
//		StavkaCenovnika.findById();
//		
		int last = fakture.size();
		
		f.broj = fakture.get(last-1).broj+1;
		f.poslovnaGodina = n.poslovnaGodina;
		f.odKoga = n.odKoga;
		f.zaKoga = n.zaKoga;
		f.narudzbenica = n;
		f.smer = "i";
		f.datum = new Date();	//Trenutno vreme
		f.datumValute = f.datum;
		f.save();

		f.ukupanIznosBezPDVa = 0.0 ;
		f.ukupanIznosPDVa = 0.0;
		f.ukupanIznosZaPlacanje = 0.0;
		
		for(models.dokumenti.StavkaNarudzbenice sn: n.stavke){
			models.dokumenti.StavkaFakture sf = new models.dokumenti.StavkaFakture();
			models.akteri.Proizvod p = models.akteri.Proizvod.findById(sn.proizvod.id);
			sf.faktura = f;
			sf.proizvod = p;
			sf.kolicina = sn.kolicina;
			sf.jedCena = sf.getPrice(p.id);//cena;
			sf.stopaPDVa = sf.getStopaPDVa(p.id);//stopaPDVa;
			sf.jedinicaMere = p.jedinicaMere;
			sf.save();
			sf.vrednost = sf.jedCena*sn.kolicina;//vrednost;
			sf.save();
			sf.osnovica = sf.vrednost;
			sf.iznosPDVa = sf.vrednost*sf.stopaPDVa/100;//iznosPDVa;
			sf.save();
			sf.ukupanIznos = sf.vrednost + sf.iznosPDVa;
			sf.save();
			f.ukupanIznosBezPDVa += sf.osnovica ;
			f.ukupanIznosPDVa += sf.iznosPDVa;
			f.ukupanIznosZaPlacanje += sf.ukupanIznos;
			f.save();
			
		}
		
	}
	
	/**
	 * Returns all entities related to specified parent. Used for 'next' functionality
	 * @param parent Name of parent entity
	 * @param id Parent entity ID
	 * @author Lazar Anđelić
	 */
	public static void getByParent(String parent, Integer id) {
		String[] packages = {"models.akteri.", "models.dokumenti."};
		Object oId = null;
		if(parent.toLowerCase().equals("poslovnipartner")) {
			parent = "Preduzece";
		}
		for(String pack: packages) {
			try {
				Class c = Class.forName(pack + parent);
				oId = c.getMethod("findById", Object.class).invoke(null, id);
				break;
			} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException | LinkageError e) {
				continue;
			}
		}
		
		if(oId == null) {
			notFound("Invalid parent table");
			return;
		}

		String typeName = oId.getClass().getTypeName();
		typeName = typeName.substring(typeName.lastIndexOf(".") + 1);
		
		List<models.dokumenti.Narudzbenica> all = models.dokumenti.Narudzbenica.find("by" + typeName, oId).fetch();
		
		List<Object> data = prepareData(all);
		
		GsonBuilder builder = new GsonBuilder();
		builder.excludeFieldsWithoutExposeAnnotation();
		builder.serializeNulls();
		Gson g = builder.create();
		
		renderJSON(data, g);	
	}
	
	private static List<Object> prepareData(List<models.dokumenti.Narudzbenica> narudzbenice) {
		if(narudzbenice.isEmpty()){
			models.dokumenti.Narudzbenica filler = new models.dokumenti.Narudzbenica();
			filler.id = null;
			filler.broj = null;
			filler.datum = null;
			filler.poslovnaGodina = new PoslovnaGodina();
			filler.odKoga = new Preduzece();
			filler.zaKoga = new Preduzece();
			
			narudzbenice.add(filler);
		}

		List<Object> data = new ArrayList<Object>();
		data.add(narudzbenice);

		HashMap<String, String> fields = new HashMap<>();
		fields.put("poslovnaGodina", "godina");
		fields.put("odKoga", "naziv");
		fields.put("zaKoga", "naziv");		
		data.add(fields);
		
		HashMap<String, String> attrTypes = new HashMap<>();
		attrTypes.put("id", "number");
		attrTypes.put("broj", "number");
		attrTypes.put("datum", "string");
		attrTypes.put("poslovnaGodina", "number");
		attrTypes.put("odKoga", "string");
		attrTypes.put("zaKoga", "string");
		data.add(attrTypes);
		
		return data;
	}

	public static void getByID(Integer id) {
		System.out.println("-- get by id --");
		models.dokumenti.Narudzbenica narudzbenice = models.dokumenti.Narudzbenica.findById(id);
		
		List<Object> data = new ArrayList<Object>();
		data.add(narudzbenice);
		
		JsonObject jog = new JsonObject();
		jog.addProperty("godina", "godina");
		data.add(jog);
		JsonObject joo = new JsonObject();
		joo.addProperty("odKoga", "naziv");
		data.add(joo);
		JsonObject joz = new JsonObject();
		joz.addProperty("zaKoga", "naziv");
		data.add(joz);
		
		GsonBuilder builder = new GsonBuilder();
		builder.excludeFieldsWithoutExposeAnnotation();
		builder.serializeNulls();
		Gson g = builder.create();
		
		renderJSON(data, g);
	}
	
	/**
	 * Inserts new entity into database.
	 * @param body
	 */
	public static void addRow(String body) {
		System.out.println("-- add row --");
		Gson g = new Gson();
		models.dokumenti.Narudzbenica n = g.fromJson(body, models.dokumenti.Narudzbenica.class);
		n.merge();
		// Return data
		
		models.dokumenti.Narudzbenica temp = models.dokumenti.Narudzbenica.find("byBroj", n.broj).first();
		
//		PoslovnaGodina god = PoslovnaGodina.findById(n.poslovnaGodina.id);
//		Preduzece od = Preduzece.findById(n.odKoga.id);
//		Preduzece za = Preduzece.findById(n.zaKoga.id);
//		n.poslovnaGodina = god;
//		n.odKoga = od;
//		n.zaKoga = za;
//		
//		List<Object> data = new ArrayList<Object>();
//		data.add(n);
//		
//		JsonObject jog = new JsonObject();
//		jog.addProperty("godina", "godina");
//		data.add(jog);
//		JsonObject joo = new JsonObject();
//		joo.addProperty("odKoga", "naziv");
//		data.add(joo);
//		JsonObject joz = new JsonObject();
//		joz.addProperty("zaKoga", "naziv");
//		data.add(joz);
		
		GsonBuilder builder = new GsonBuilder();
		builder.excludeFieldsWithoutExposeAnnotation();
		builder.serializeNulls();
		Gson gr = builder.create();
		
		renderJSON(temp, gr);
	}
	
	/**
	 * Updates existing entity with given object.
	 * 
	 * @param body
	 */
	public static void updateRow(String body) {
		System.out.println("-- update row --");
		Gson g = new Gson();

		models.dokumenti.Narudzbenica n = g.fromJson(body, models.dokumenti.Narudzbenica.class);
		models.dokumenti.Narudzbenica n1 = models.dokumenti.Narudzbenica.findById(n.id);
		
		n1.broj = n.broj;
		n1.datum = n.datum;
		n1.poslovnaGodina = n.poslovnaGodina;
		n1.odKoga = n.odKoga;
		n1.zaKoga = n.zaKoga;
		n1.save();
	}
	
	/**
	 * Deletes the specified entity.
	 * @param id
	 */
	public static void deleteRow(Integer id) {
		models.dokumenti.Narudzbenica n = models.dokumenti.Narudzbenica.findById(id);
		n.delete();
	}
//	private static Gson getGson() {
//		GsonBuilder builder = new GsonBuilder();
//		builder.excludeFieldsWithoutExposeAnnotation();
//		builder.serializeNulls();
//		Gson g = builder.create();		
//		return g;
//	}
//
//	private static List<Object> fillData(List<models.dokumenti.Narudzbenica> narudzbenice) {
//		List<Object> data = new ArrayList<Object>();
//		
//		data.add(narudzbenice);
//		
//		JsonObject jog = new JsonObject();
//		jog.addProperty("godina", "godina");
//		data.add(jog);
//		JsonObject joo = new JsonObject();
//		joo.addProperty("odKoga", "naziv");
//		data.add(joo);
//		JsonObject joz = new JsonObject();
//		joz.addProperty("zaKoga", "naziv");
//		data.add(joz);		
//		
//		return data;
//	}
//	
	public static void getNextTables() {
		Field[] fieldList = models.dokumenti.Narudzbenica.class.getFields();
		List<String> nextTables = new ArrayList<>();
		
		for(Field f: fieldList) {
			String name = f.getType().getName();
			if(name.contains("java.util.List")) {
				name = f.getGenericType().getTypeName();
				name = name.substring(name.lastIndexOf(".") + 1, name.lastIndexOf(">"));
				nextTables.add(name);
			}
		}
		
		renderJSON(nextTables);
	}
}
