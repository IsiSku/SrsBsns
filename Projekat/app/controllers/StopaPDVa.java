package controllers;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.hibernate.tool.hbm2x.StringUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import models.akteri.PDV;

/**
 * Controller for {@link models.akteri.PDV}.
 * Manages CRUD and standard form actions.
 * 
 * @author Isidora Skulec
 */
public class StopaPDVa extends GenericController {
	
	public static void getAll() {
		List<models.akteri.StopaPDVa> all = models.akteri.StopaPDVa.findAll();

		List<Object> data = prepareData(all);
		
		GsonBuilder builder = new GsonBuilder();
		builder.excludeFieldsWithoutExposeAnnotation();
		builder.serializeNulls();
		Gson g = builder.create();
		
		renderJSON(data, g);
	}
	
	public static void getByParent(String parent, Integer id) {
		String[] packages = {"models.akteri.", "models.dokumenti."};
		Object oId = null;
		for(String pack: packages) {
			try {
				Class c = Class.forName(pack + parent);
				oId = c.getMethod("findById", Object.class).invoke(null, id);
				break;
			} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException | LinkageError e) {
				continue;
			}
		}
		
		if(oId == null) {
			notFound("Invalid parent table");
			return;
		}

		String typeName = oId.getClass().getTypeName();
		typeName = typeName.substring(typeName.lastIndexOf(".") + 1);
		
		List<models.akteri.StopaPDVa> all = models.akteri.StopaPDVa.find("by" + typeName, oId).fetch();
		
		List<Object> data = prepareData(all);
		
		GsonBuilder builder = new GsonBuilder();
		builder.excludeFieldsWithoutExposeAnnotation();
		builder.serializeNulls();
		Gson g = builder.create();
		
		renderJSON(data, g);	
	}
	
	private static List<Object> prepareData(List<models.akteri.StopaPDVa> all) {
		if(all.isEmpty()) {
			models.akteri.StopaPDVa filler = new models.akteri.StopaPDVa();
			filler.id = null;
			filler.procenat = null;
			filler.datum = null;
			filler.pdv = new PDV();
			
			all.add(filler);
		}
		
		List<Object> data = new ArrayList<Object>();
		data.add(all);
		
		HashMap<String, String> fields = new HashMap<>();
		fields.put("pdv", "naziv");
		data.add(fields);
		
		HashMap<String, String> attrTypes = new HashMap<>();
		attrTypes.put("id", "number");
		attrTypes.put("procenat", "number");
		attrTypes.put("datum", "string");
		attrTypes.put("pdv", "string");		
		data.add(attrTypes);
		
		return data;
	}
	
	public static void getByID(Integer id) {
		System.out.println("-- get by id --");
		models.akteri.StopaPDVa grupa = models.akteri.StopaPDVa.findById(id);
		
		List<Object> data = new ArrayList<Object>();
		data.add(grupa);
		JsonObject pn = new JsonObject();		pn.addProperty("pdv", "naziv");		data.add(pn);	// PDV
		
		GsonBuilder builder = new GsonBuilder();
		builder.excludeFieldsWithoutExposeAnnotation();
		builder.serializeNulls();
		Gson g = builder.create();
		
		renderJSON(data, g);
	}
	
	public static void addRow(String body) {
		System.out.println("-- add row --");
		Gson g = new Gson();
		models.akteri.StopaPDVa p = g.fromJson(body, models.akteri.StopaPDVa.class);
		
		p.merge();
		
		// Return data
		
		PDV pdv = PDV.findById(p.pdv.id);
		p.pdv = pdv;
		
		List<Object> data = new ArrayList<Object>();
		data.add(p);
		JsonObject pn = new JsonObject();		pn.addProperty("pdv", "naziv");		data.add(pn);	// PDV
		
		GsonBuilder builder = new GsonBuilder();
		builder.excludeFieldsWithoutExposeAnnotation();
		builder.serializeNulls();
		Gson gr = builder.create();
		
		renderJSON(data, gr);

	}
	
	public static void updateRow(String body) {
		System.out.println("-- update row --");
		Gson g = new Gson();
		models.akteri.StopaPDVa p = g.fromJson(body, models.akteri.StopaPDVa.class);
		models.akteri.StopaPDVa po = models.akteri.StopaPDVa.findById(p.id);
		
		po.procenat = p.procenat;
		po.datum = p.datum;
		po.pdv = p.pdv;
		
		po.save();
	}
	
	public static void deleteRow(Integer id) {
		models.akteri.StopaPDVa c = models.akteri.StopaPDVa.findById(id);
		c.delete();
		ok();
		
	}

	public static void getNextTables() {
		Field[] fieldList = models.akteri.StopaPDVa.class.getFields();
		List<String> nextTables = new ArrayList<>();
		
		for(Field f: fieldList) {
			String name = f.getType().getName();
			if(name.contains("java.util.List")) {
				name = f.getGenericType().getTypeName();
				name = name.substring(name.lastIndexOf(".") + 1, name.lastIndexOf(">"));
				nextTables.add(name);
			}
		}
		
		renderJSON(nextTables);
	}
}
