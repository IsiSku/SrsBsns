package controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import models.akteri.Preduzece;

/**
 * Created by Nemanja on 24/6/2016.
 */
public class NasePreduzece extends GenericController {

	public static void getPreduzece() {
		Preduzece preduzece = Preduzece.find("byNasePreduzece", true).first();

		List<Object> data = new ArrayList<Object>();
		data.add(preduzece);
		
		HashMap<String, String> fields = new HashMap<String, String>();
		fields.put("mesto", "naziv");
		data.add(fields);
		
		HashMap<String, String> attrTypes = new HashMap<String, String>();
		attrTypes.put("id", "number");
		attrTypes.put("naziv", "string");
		attrTypes.put("PIB", "string");
		attrTypes.put("maticniBroj", "string");
		attrTypes.put("ulica", "string");
		attrTypes.put("brojUlice", "number");
		attrTypes.put("brojTelefona", "string");
		attrTypes.put("email", "string");
		attrTypes.put("web", "string");
		attrTypes.put("nasePreduzece", "string");
		attrTypes.put("mesto", "string");
		data.add(attrTypes);
		
		GsonBuilder builder = new GsonBuilder();
		builder.excludeFieldsWithoutExposeAnnotation();
		builder.serializeNulls();
		Gson g = builder.create();

		renderJSON(data, g);
	}

	public static void updateRow(String body) {
		PoslovniPartner.updateRow(body);
	}

}
