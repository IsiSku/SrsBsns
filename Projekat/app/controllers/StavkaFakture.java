package controllers;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import models.akteri.Cenovnik;
import models.akteri.GrupaProizvoda;
import models.akteri.JedinicaMere;
import models.akteri.PDV;
import models.akteri.Proizvod;
import models.akteri.StavkaCenovnika;
import models.dokumenti.Faktura;
import play.db.DB;

public class StavkaFakture extends GenericController {
	
	private static String PDV_STATEMENT = "SELECT id, max(datum), procenat, pdv FROM StopePDVa WHERE pdv=? GROUP BY id, procenat, pdv";
	private static String CENOVNIK_STATEMENT = "SELECT id, max(datumVazenja) FROM Cenovnik";
	
	private static double getStopaPDVa(Integer prID){
		Double stopa = null;
		Proizvod proizvod = Proizvod.findById(prID);
		GrupaProizvoda grupaProizvoda = proizvod.grupaProizvoda;
		PDV pdv = grupaProizvoda.pdv;
		
		PreparedStatement ps;
		try {
			ps = DB.getConnection().prepareStatement(PDV_STATEMENT);
			ps.setInt(1, pdv.id);
			ResultSet resPDV = ps.executeQuery();
			resPDV.next();
			
			Integer stopaID = resPDV.getInt(1);
			Date stopaDatum = resPDV.getDate(2);
			Double stopaProcenat = resPDV.getDouble(3);
			stopa = stopaProcenat;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return stopa;
	}
	
	private static double getPrice(Integer prID) {
		double cena = 0;
		
		//PDV
		Proizvod proizvod = Proizvod.findById(prID);
//		GrupaProizvoda grupaProizvoda = proizvod.grupaProizvoda;
//		PDV pdv = grupaProizvoda.pdv;
		
		//CENA
		
		
		try {
			// StopaPDVa
//			PreparedStatement ps = DB.getConnection().prepareStatement(PDV_STATEMENT);
//			ps.setInt(1, pdv.id);
//			ResultSet resPDV = ps.executeQuery();
//			resPDV.next();
//			
//			Integer stopaID = resPDV.getInt(1);
//			Date stopaDatum = resPDV.getDate(2);
//			Double stopaProcenat = resPDV.getDouble(3);
			
			// Cenovnik
			PreparedStatement cs = DB.getConnection().prepareStatement(CENOVNIK_STATEMENT);
			ResultSet resCenovnik = cs.executeQuery();
			resCenovnik.next();
			
			Integer cenovnikID = resCenovnik.getInt(1);
			Date cenovnikDatum = resCenovnik.getDate(2);
			Cenovnik cenovnik = Cenovnik.findById(cenovnikID);
			
			StavkaCenovnika stavkaCenovnika = (StavkaCenovnika) StavkaCenovnika.find("byCenovnikAndProizvod", cenovnik, proizvod).fetch().get(0);
			cena = stavkaCenovnika.cena;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return cena;
	}

	public static void getAll() {
		List<models.dokumenti.StavkaFakture> all = models.dokumenti.StavkaFakture.findAll();

		List<Object> data = prepareData(all);

		GsonBuilder builder = new GsonBuilder();
		builder.excludeFieldsWithoutExposeAnnotation();
		builder.serializeNulls();
		Gson g = builder.create();
		
		renderJSON(data, g);
	
	}
	/**
	 * Returns all entities related to specified parent. Used for 'next' functionality
	 * @param parent Name of parent entity
	 * @param id Parent entity ID
	 * @author Lazar Anđelić
	 */
	public static void getByParent(String parent, Integer id) {
		String[] packages = {"models.akteri.", "models.dokumenti."};
		Object oId = null;
		if(parent.toLowerCase().equals("poslovnipartner")) {
			parent = "Preduzece";
		}
		for(String pack: packages) {
			try {
				Class c = Class.forName(pack + parent);
				oId = c.getMethod("findById", Object.class).invoke(null, id);
				break;
			} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException | LinkageError e) {
				continue;
			}
		}
		
		if(oId == null) {
			notFound("Invalid parent table");
			return;
		}

		String typeName = oId.getClass().getTypeName();
		typeName = typeName.substring(typeName.lastIndexOf(".") + 1);
		
		List<models.dokumenti.StavkaFakture> all = models.dokumenti.StavkaFakture.find("by" + typeName, oId).fetch();
		
		List<Object> data = prepareData(all);
		
		GsonBuilder builder = new GsonBuilder();
		builder.excludeFieldsWithoutExposeAnnotation();
		builder.serializeNulls();
		Gson g = builder.create();
		
		renderJSON(data, g);	
	}
	
	private static List<Object> prepareData(List<models.dokumenti.StavkaFakture> all) {
		if(all.isEmpty()) {
			models.dokumenti.StavkaFakture filler = new models.dokumenti.StavkaFakture();
			filler.id = null;
			filler.iznosPDVa = null;
			filler.iznosRabata = null;
			filler.jedCena = null;
			filler.jedinicaMere = null;//new JedinicaMere();
			filler.kolicina = null;
			filler.osnovica = null;
			filler.stopaPDVa = null;
			filler.stopaRabata = null;
			filler.ukupanIznos = null;
			filler.vrednost = null;
			filler.faktura = new Faktura();
			filler.proizvod = new Proizvod();
			
			all.add(filler);
		}

		List<Object> data = new ArrayList<Object>();
		data.add(all);

		HashMap<String, String> fields = new HashMap<>();
		fields.put("jedinicaMere", "naziv");
		fields.put("faktura", "broj");
		fields.put("proizvod", "naziv");		
		data.add(fields);
		
		HashMap<String, String> attrTypes = new HashMap<>();
		attrTypes.put("id", "number");
		attrTypes.put("iznosPDVa", "number");
		attrTypes.put("iznosRabata", "number");
		attrTypes.put("jedCena", "number");
		attrTypes.put("kolicina", "number");
		attrTypes.put("osnovica", "number");
		attrTypes.put("stopaPDVa", "number");
		attrTypes.put("stopaRabata", "number");
		attrTypes.put("ukupanIznos", "number");
		attrTypes.put("vrednost", "number");
		attrTypes.put("jedinicaMere", "string");
		attrTypes.put("faktura", "number");
		attrTypes.put("proizvod", "string");
		data.add(attrTypes);
		
		return data;
	}
	
	public static void getByID(Integer id) {
		System.out.println("-- get by id --");
		models.dokumenti.StavkaFakture grupa = models.dokumenti.StavkaFakture.findById(id);
		
		List<Object> data = new ArrayList<Object>();
		data.add(grupa);
		JsonObject jof = new JsonObject();
		jof.addProperty("faktura", "broj");
		data.add(jof);
		JsonObject jop = new JsonObject();
		jop.addProperty("proizvod", "naziv");
		data.add(jop);

		GsonBuilder builder = new GsonBuilder();
		builder.excludeFieldsWithoutExposeAnnotation();
		builder.serializeNulls();
		Gson g = builder.create();
		
		renderJSON(data, g);
	}
	
	public static void addRow(String body) {
		System.out.println("-- add row --");
		Gson g = new Gson();
		models.dokumenti.StavkaFakture sf = g.fromJson(body, models.dokumenti.StavkaFakture.class);
		
		sf.merge();
		
		// Return data
		Faktura fak = Faktura.findById(sf.faktura.id);
		sf.faktura = fak;
		
		Proizvod pro = Proizvod.findById(sf.proizvod.id);
		sf.proizvod = pro;
		
		
		List<Object> data = new ArrayList<Object>();
		data.add(sf);
		JsonObject jof = new JsonObject();
		jof.addProperty("faktura", "broj");
		data.add(jof);
		JsonObject jop = new JsonObject();
		jop.addProperty("proizvod", "naziv");
		data.add(jop);

		GsonBuilder builder = new GsonBuilder();
		builder.excludeFieldsWithoutExposeAnnotation();
		builder.serializeNulls();
		Gson gr = builder.create();
		
		renderJSON(data, gr);
	}
	
	public static void updateRow(String body) {
		System.out.println("-- update row --");
		Gson g = new Gson();
		models.dokumenti.StavkaFakture f = g.fromJson(body, models.dokumenti.StavkaFakture.class);
		models.dokumenti.StavkaFakture f1 = models.dokumenti.StavkaFakture.findById(f.id);
		
		f1.iznosPDVa = f.iznosPDVa;
		f1.iznosRabata = f.iznosRabata;
		f1.jedCena = f.jedCena;
		f1.jedinicaMere = f.jedinicaMere;
		f1.kolicina = f.kolicina;
		f1.osnovica = f.osnovica;
		f1.stopaPDVa = f.stopaPDVa;
		f1.stopaRabata = f.stopaRabata;
		f1.ukupanIznos = f.ukupanIznos;
		f1.vrednost = f.vrednost;
		f1.faktura = f.faktura;
		f1.proizvod = f.proizvod;
		f1.save();
	}
	
	public static void deleteRow(Integer id) {
		models.dokumenti.StavkaFakture f = models.dokumenti.StavkaFakture.findById(id);
		f.delete();
		ok();
	}
	
	public static void getNextTables() {
		Field[] fieldList = models.dokumenti.StavkaFakture.class.getFields();
		List<String> nextTables = new ArrayList<>();
		
		for(Field f: fieldList) {
			String name = f.getType().getName();
			if(name.contains("java.util.List")) {
				name = f.getGenericType().getTypeName();
				name = name.substring(name.lastIndexOf(".") + 1, name.lastIndexOf(">"));
				nextTables.add(name);
			}
		}
		
		renderJSON(nextTables);
	}
	
}
