package controllers;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import models.akteri.GrupaProizvoda;
import models.akteri.Mesto;
import models.akteri.PDV;
import models.akteri.Preduzece;
import models.akteri.StopaPDVa;
import play.mvc.Controller;

/**
 * Controller for {@link models.akteri.PoslovnaGodina}.
 * Manages CRUD and standard form actions.
 * 
 * @author Isidora Skulec
 */
public class PoslovnaGodina extends GenericController {
	
	public static void getAll() {
		List<models.akteri.PoslovnaGodina> all = models.akteri.PoslovnaGodina.findAll();

		List<Object> data = prepareData(all);

		GsonBuilder builder = new GsonBuilder();
		builder.excludeFieldsWithoutExposeAnnotation();
		builder.serializeNulls();
		Gson g = builder.create();
		
		renderJSON(data, g);
	}
	/**
	 * Returns all entities related to specified parent. Used for 'next' functionality
	 * @param parent Name of parent entity
	 * @param id Parent entity ID
	 * @author Lazar Anđelić
	 */
	public static void getByParent(String parent, Integer id) {
		String[] packages = {"models.akteri.", "models.dokumenti."};
		Object oId = null;
		if(parent.toLowerCase().equals("poslovnipartner")) {
			parent = "Preduzece";
		}
		for(String pack: packages) {
			try {
				Class c = Class.forName(pack + parent);
				oId = c.getMethod("findById", Object.class).invoke(null, id);
				break;
			} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException | LinkageError e) {
				continue;
			}
		}
		
		if(oId == null) {
			notFound("Invalid parent table");
			return;
		}

		String typeName = oId.getClass().getTypeName();
		typeName = typeName.substring(typeName.lastIndexOf(".") + 1);
		
		List<models.akteri.PoslovnaGodina> all = models.akteri.PoslovnaGodina.find("by" + typeName, oId).fetch();
		
		List<Object> data = prepareData(all);
		
		GsonBuilder builder = new GsonBuilder();
		builder.excludeFieldsWithoutExposeAnnotation();
		builder.serializeNulls();
		Gson g = builder.create();
		
		renderJSON(data, g);	
	}
	
	private static List<Object> prepareData(List<models.akteri.PoslovnaGodina> all) {
		if(all.isEmpty()) {
			models.akteri.PoslovnaGodina filler = new models.akteri.PoslovnaGodina();
			filler.id = null;
			filler.godina = null;
			filler.zakljucena = null;
			filler.preduzece = new Preduzece();
			
			all.add(filler);
		}
		
		List<Object> data = new ArrayList<Object>();
		data.add(all);
		
		HashMap<String, String> fields = new HashMap<>();
		fields.put("preduzece", "naziv");
		data.add(fields);

		HashMap<String, String> attrTypes = new HashMap<>();
		attrTypes.put("id", "number");
		attrTypes.put("godina", "number");
		attrTypes.put("zakljucena", "boolean");	
		attrTypes.put("preduzece", "string");		
		data.add(attrTypes);
		
		return data;
	}
	
	public static void addRow(String body) {
		System.out.println("-- add row --");
		Gson g = new Gson();
		models.akteri.PoslovnaGodina p = g.fromJson(body, models.akteri.PoslovnaGodina.class);
		
		p.merge();
		
		// Return data
		
		Preduzece m = Preduzece.findById(p.preduzece.id);
		p.preduzece = m;
		
		List<Object> data = new ArrayList<Object>();
		data.add(p);
		JsonObject mn = new JsonObject();		mn.addProperty("preduzece", "naziv");		data.add(mn);	// preduzece
		
		GsonBuilder builder = new GsonBuilder();
		builder.excludeFieldsWithoutExposeAnnotation();
		builder.serializeNulls();
		Gson gr = builder.create();
		
		renderJSON(data, gr);
	}
	
	public static void updateRow(String body) {
		System.out.println("-- update row --");
		Gson g = new Gson();
		models.akteri.PoslovnaGodina p = g.fromJson(body, models.akteri.PoslovnaGodina.class);
		models.akteri.PoslovnaGodina po = models.akteri.PoslovnaGodina.findById(p.id);
		
		po.godina = p.godina;
		po.zakljucena = p.zakljucena;
		po.preduzece = p.preduzece;
		
		po.save();
	}
	
	public static void deleteRow(Integer id) {
		models.akteri.PoslovnaGodina c = models.akteri.PoslovnaGodina.findById(id);
		c.delete();
		ok();
		
	}

	public static void getNextTables() {
		Field[] fieldList = models.akteri.PoslovnaGodina.class.getFields();
		List<String> nextTables = new ArrayList<>();
		
		for(Field f: fieldList) {
			String name = f.getType().getName();
			if(name.contains("java.util.List")) {
				name = f.getGenericType().getTypeName();
				name = name.substring(name.lastIndexOf(".") + 1, name.lastIndexOf(">"));
				nextTables.add(name);
			}
		}
		
		renderJSON(nextTables);
	}
}
