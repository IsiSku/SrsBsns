package controllers;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import models.akteri.GrupaProizvoda;
import play.db.jpa.GenericModel;
import play.mvc.Controller;

/**
 * 
 * @author Isidora Skulec
 */
public class GenericController extends Controller {
	
	protected static <T extends GenericModel> void _getAll(Class<T> m, List<T> all, HashMap<String, String> attrTypes) {
		try {
			List<Object> data = new ArrayList<Object>();
			if(all.isEmpty()) {
				T filler = m.newInstance();
				all.add(filler);
				data.add(all);		
				data.add(attrTypes);
			}
			else {
				data.add(all);
			}
			
			GsonBuilder builder = new GsonBuilder();
			builder.excludeFieldsWithoutExposeAnnotation();
			builder.serializeNulls();
			Gson g = builder.create();
			
			renderJSON(data, g);
		
		} catch (InstantiationException e) {
			e.printStackTrace();
			error();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
			error();
		}
	}
	
	/**
	 * @author Lazar Andjelic
	 */
//	public static void getNextTables() {
//		Field[] fieldList = GrupaProizvoda.class.getFields();
//		List<String> nextTables = new ArrayList<>();
//		
//		for(Field f: fieldList) {
//			String name = f.getType().getName();
//			if(name.contains("java.util.List")) {
//				name = f.getGenericType().getTypeName();
//				name = name.substring(name.lastIndexOf(".") + 1, name.lastIndexOf(">") - 1);
//				nextTables.add(name);
//			}
//		}
//		
//		renderJSON(nextTables);
//	}

}
