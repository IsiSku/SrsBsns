package controllers;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import models.akteri.PDV;
import models.akteri.Preduzece;
import models.akteri.Proizvod;

/**
 * Controller for {@link models.akteri.GrupaProizvoda}.
 * Manages CRUD and standard form actions.
 * 
 * @author Isidora Skulec
 */
public class GrupaProizvoda extends GenericController {

	public static void getAll() {
		List<models.akteri.GrupaProizvoda> all = models.akteri.GrupaProizvoda.findAll();

		List<Object> data = prepareData(all);
		
		GsonBuilder builder = new GsonBuilder();
		builder.excludeFieldsWithoutExposeAnnotation();
		builder.serializeNulls();
		Gson g = builder.create();
		
		renderJSON(data, g);
	}
	/**
	 * Returns all entities related to specified parent. Used for 'next' functionality
	 * @param parent Name of parent entity
	 * @param id Parent entity ID
	 * @author Lazar Anđelić
	 */
	public static void getByParent(String parent, Integer id) {
		String[] packages = {"models.akteri.", "models.dokumenti."};
		Object oId = null;
		if(parent.toLowerCase().equals("poslovnipartner")) {
			parent = "Preduzece";
		}
		for(String pack: packages) {
			try {
				Class c = Class.forName(pack + parent);
				oId = c.getMethod("findById", Object.class).invoke(null, id);
				break;
			} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException | LinkageError e) {
				continue;
			}
		}
		
		if(oId == null) {
			notFound("Invalid parent table");
			return;
		}

		String typeName = oId.getClass().getTypeName();
		typeName = typeName.substring(typeName.lastIndexOf(".") + 1);
		
		List<models.akteri.GrupaProizvoda> all = models.akteri.GrupaProizvoda.find("by" + typeName, oId).fetch();
		
		List<Object> data = prepareData(all);
		
		GsonBuilder builder = new GsonBuilder();
		builder.excludeFieldsWithoutExposeAnnotation();
		builder.serializeNulls();
		Gson g = builder.create();
		
		renderJSON(data, g);	
	}
	
	private static List<Object> prepareData(List<models.akteri.GrupaProizvoda> all) {
		if(all.isEmpty()) {
			models.akteri.GrupaProizvoda filler = new models.akteri.GrupaProizvoda();
			filler.id = null;
			filler.naziv = null;
			filler.pdv = new PDV();
			
			all.add(filler);
		}

		List<Object> data = new ArrayList<Object>();
		data.add(all);

		HashMap<String, String> fields = new HashMap<String, String>();
		fields.put("pdv", "naziv");
		data.add(fields);
		
		HashMap<String, String> attrTypes = new HashMap<>();
		attrTypes.put("id", "number");		// tip za ID
		attrTypes.put("naziv", "string");	// tip za naziv
		attrTypes.put("pdv", "string");	// tip za naziv
		data.add(attrTypes);
		
		return data;
	}
	
	public static void getByID(Integer id) {
		System.out.println("-- get by id --");
		models.akteri.GrupaProizvoda grupa = models.akteri.GrupaProizvoda.findById(id);
		
		List<Object> data = new ArrayList<Object>();
		data.add(grupa);
		JsonObject pn = new JsonObject();		pn.addProperty("pdv", "naziv");		data.add(pn);	// PDV
		
		GsonBuilder builder = new GsonBuilder();
		builder.excludeFieldsWithoutExposeAnnotation();
		builder.serializeNulls();
		Gson g = builder.create();
		
		renderJSON(data, g);
	}

	public static void addRow(String body) {
		System.out.println("-- add row --");
		Gson g = new Gson();
		models.akteri.GrupaProizvoda p = g.fromJson(body, models.akteri.GrupaProizvoda.class);
		
		p.merge();
		
		// Return data
		
		PDV pdv = PDV.findById(p.pdv.id);
		p.pdv = pdv;
		
		List<Object> data = new ArrayList<Object>();
		data.add(p);
		JsonObject pn = new JsonObject();		pn.addProperty("pdv", "naziv");		data.add(pn);	// PDV
		
		GsonBuilder builder = new GsonBuilder();
		builder.excludeFieldsWithoutExposeAnnotation();
		builder.serializeNulls();
		Gson gr = builder.create();
		
		renderJSON(data, gr);

	}

	public static void updateRow(String body) {
		System.out.println("-- update row --");
		Gson g = new Gson();
		models.akteri.GrupaProizvoda p = g.fromJson(body, models.akteri.GrupaProizvoda.class);
		models.akteri.GrupaProizvoda po = models.akteri.GrupaProizvoda.findById(p.id);
		
		po.naziv = p.naziv;
		//po.proizvodi = p.proizvodi;
		po.pdv = p.pdv;
		po.save();
	}

	public static void deleteRow(Integer id) {
		models.akteri.GrupaProizvoda p = models.akteri.GrupaProizvoda.findById(id);
		
		List<Proizvod> pp = p.proizvodi;
		if(pp.isEmpty()) {
			p.delete();
			ok();
		}
		else {
			forbidden("Cenovnik has child entities [proizvodi]");
		}
	}

	public static void getNextTables() {
		Field[] fieldList = models.akteri.GrupaProizvoda.class.getFields();
		List<String> nextTables = new ArrayList<>();
		
		for(Field f: fieldList) {
			String name = f.getType().getName();
			if(name.contains("java.util.List")) {
				name = f.getGenericType().getTypeName();
				name = name.substring(name.lastIndexOf(".") + 1, name.lastIndexOf(">"));
				nextTables.add(name);
			}
		}
		
		renderJSON(nextTables);
	}
}
