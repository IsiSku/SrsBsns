package controllers;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import models.akteri.Preduzece;
import models.akteri.StavkaCenovnika;

/**
 * Controller for {@link models.akteri.Cenovnik}.
 * Manages CRUD and standard form actions.
 * 
 * @author Isidora Skulec
 */
public class Cenovnik extends GenericController {
	
	public static void getAll() {
		List<models.akteri.Cenovnik> all = models.akteri.Cenovnik.findAll();

		List<Object> data = prepareData(all);
		
		GsonBuilder builder = new GsonBuilder();
		builder.excludeFieldsWithoutExposeAnnotation();
		builder.serializeNulls();
		Gson g = builder.create();
		
		renderJSON(data, g);
	}
	/**
	 * Returns all entities related to specified parent. Used for 'next' functionality
	 * @param parent Name of parent entity
	 * @param id Parent entity ID
	 * @author Lazar Anđelić
	 */
	public static void getByParent(String parent, Integer id) {
		String[] packages = {"models.akteri.", "models.dokumenti."};
		Object oId = null;
		if(parent.toLowerCase().equals("poslovnipartner")) {
			parent = "Preduzece";
		}
		for(String pack: packages) {
			try {
				Class c = Class.forName(pack + parent);
				oId = c.getMethod("findById", Object.class).invoke(null, id);
				break;
			} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException | LinkageError e) {
				continue;
			}
		}
		
		if(oId == null) {
			notFound("Invalid parent table");
			return;
		}

		String typeName = oId.getClass().getTypeName();
		typeName = typeName.substring(typeName.lastIndexOf(".") + 1);
		
		List<models.akteri.Cenovnik> all = models.akteri.Cenovnik.find("by" + typeName, oId).fetch();
		
		List<Object> data = prepareData(all);
		
		GsonBuilder builder = new GsonBuilder();
		builder.excludeFieldsWithoutExposeAnnotation();
		builder.serializeNulls();
		Gson g = builder.create();
		
		renderJSON(data, g);	
	}
	
	private static List<Object> prepareData(List<models.akteri.Cenovnik> all) {
		if(all.isEmpty()) {
			models.akteri.Cenovnik filler = new models.akteri.Cenovnik();
			filler.id = null;
			filler.datumVazenja = null;
			filler.preduzece = new Preduzece();
			
			all.add(filler);
		}

		List<Object> data = new ArrayList<Object>();
		data.add(all);

		HashMap<String, String> fields = new HashMap<String, String>();
		fields.put("preduzece", "naziv");
		data.add(fields);

		HashMap<String, String> attrTypes = new HashMap<>();
		attrTypes.put("id", "number");		// tip za ID
		attrTypes.put("datumVazenja", "string");// datum cemo prikazivati kao string unutar tabele
		attrTypes.put("preduzece", "string");// datum cemo prikazivati kao string unutar tabele
		data.add(attrTypes);
		
		return data;
	}
	
	public static void getByID(Integer id) {
		System.out.println("-- get by id --");
		models.akteri.Cenovnik grupa = models.akteri.Cenovnik.findById(id);
		
		List<Object> data = new ArrayList<Object>();
		data.add(grupa);
		JsonObject pn = new JsonObject();		pn.addProperty("preduzece", "naziv");		data.add(pn);	// preduzece
		
		GsonBuilder builder = new GsonBuilder();
		builder.excludeFieldsWithoutExposeAnnotation();
		builder.serializeNulls();
		Gson g = builder.create();
		
		renderJSON(data, g);
	}
	
	public static void addRow(String body) {
		System.out.println("-- add row --");
		Gson g = new Gson();
		models.akteri.Cenovnik p = g.fromJson(body, models.akteri.Cenovnik.class);
		
		p.merge();
		
		// Return data
		
		Preduzece pred = Preduzece.findById(p.preduzece.id);
		p.preduzece = pred;
		
		List<Object> data = new ArrayList<Object>();
		data.add(p);
		JsonObject pn = new JsonObject();		pn.addProperty("preduzece", "naziv");		data.add(pn);	// preduzece
		
		GsonBuilder builder = new GsonBuilder();
		builder.excludeFieldsWithoutExposeAnnotation();
		builder.serializeNulls();
		Gson gr = builder.create();
		
		renderJSON(data, gr);

	}
	
	public static void updateRow(String body) {
		System.out.println("-- update row --");
		Gson g = new Gson();
		models.akteri.Cenovnik p = g.fromJson(body, models.akteri.Cenovnik.class);
		models.akteri.Cenovnik po = models.akteri.Cenovnik.findById(p.id);
		
		po.datumVazenja = p.datumVazenja;
		po.preduzece = p.preduzece;
		//po.stavkeCenovnika = p.stavkeCenovnika;
		po.save();
	}
	
	public static void deleteRow(Integer id) {
		models.akteri.Cenovnik c = models.akteri.Cenovnik.findById(id);
		
		List<StavkaCenovnika> sc = c.stavkeCenovnika;
		if(sc.isEmpty()) {
			c.delete();
		}
		else {
			forbidden("Cenovnik has child entities [stavke]");
		}
	}

	public static void getNextTables() {
		Field[] fieldList = models.akteri.Cenovnik.class.getFields();
		List<String> nextTables = new ArrayList<>();
		
		for(Field f: fieldList) {
			String name = f.getType().getName();
			if(name.contains("java.util.List")) {
				name = f.getGenericType().getTypeName();
				name = name.substring(name.lastIndexOf(".") + 1, name.lastIndexOf(">"));
				nextTables.add(name);
			}
		}
		
		renderJSON(nextTables);
	}
}
