package controllers;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import models.akteri.GrupaProizvoda;
import models.akteri.JedinicaMere;
import models.akteri.PDV;
import models.akteri.Preduzece;
import play.mvc.Controller;

/**
 * Controller for {@link models.akteri.Proizvod} entities.
 * Manages CRUD and standard form actions.
 * 
 * @author Isidora Skulec
 */
public class Proizvod extends GenericController {
	
	public static void getAll() {
		List<models.akteri.Proizvod> all = models.akteri.Proizvod.findAll();

		List<Object> data = prepareData(all);
		
		GsonBuilder builder = new GsonBuilder();
		builder.excludeFieldsWithoutExposeAnnotation();
		builder.serializeNulls();
		Gson g = builder.create();
		
		renderJSON(data, g);
	}
	/**
	 * Returns all entities related to specified parent. Used for 'next' functionality
	 * @param parent Name of parent entity
	 * @param id Parent entity ID
	 * @author Lazar Anđelić
	 */
	public static void getByParent(String parent, Integer id) {
		String[] packages = {"models.akteri.", "models.dokumenti."};
		Object oId = null;
		if(parent.toLowerCase().equals("poslovnipartner")) {
			parent = "Preduzece";
		}
		for(String pack: packages) {
			try {
				Class c = Class.forName(pack + parent);
				oId = c.getMethod("findById", Object.class).invoke(null, id);
				break;
			} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException | LinkageError e) {
				continue;
			}
		}
		
		if(oId == null) {
			notFound("Invalid parent table");
			return;
		}

		String typeName = oId.getClass().getTypeName();
		typeName = typeName.substring(typeName.lastIndexOf(".") + 1);
		
		List<models.akteri.Proizvod> all = models.akteri.Proizvod.find("by" + typeName, oId).fetch();
		
		List<Object> data = prepareData(all);
		
		GsonBuilder builder = new GsonBuilder();
		builder.excludeFieldsWithoutExposeAnnotation();
		builder.serializeNulls();
		Gson g = builder.create();
		
		renderJSON(data, g);	
	}
	
	private static List<Object> prepareData(List<models.akteri.Proizvod> all) {
		if(all.isEmpty()) {
			models.akteri.Proizvod filler = new models.akteri.Proizvod();
			filler.id = null;
			filler.naziv = null;
			filler.tip = null;
			filler.jedinicaMere = new JedinicaMere();
			filler.grupaProizvoda = new GrupaProizvoda();
			filler.preduzece = new Preduzece();
			
			all.add(filler);
		}

		List<Object> data = new ArrayList<Object>();		
		data.add(all);

		HashMap<String, String> fields = new HashMap<String, String>();
		fields.put("jedinicaMere", "naziv");	// jedinica mere
		fields.put("grupaProizvoda", "naziv");	// grupa proizvoda
		fields.put("preduzece", "naziv");		// preduzece			
		data.add(fields);

		HashMap<String, String> attrTypes = new HashMap<>();
		attrTypes.put("id", "number");
		attrTypes.put("naziv", "string");
		attrTypes.put("jedinicaMere", "string");	
		attrTypes.put("grupaProizvoda", "string");	
		attrTypes.put("preduzece", "string");			
		data.add(attrTypes);
		
		return data;
	}
	
	public static void getById(Integer id) {
		models.akteri.Proizvod all = models.akteri.Proizvod.findById(id);
		
		List<Object> data = new ArrayList<Object>();
		data.add(all);
		JsonObject pn = new JsonObject();		pn.addProperty("preduzece", "naziv");			data.add(pn);	// preduzece
		JsonObject jm = new JsonObject();		jm.addProperty("jedinicaMere", "naziv");		data.add(jm);	// jedinica mere
		JsonObject gp = new JsonObject();		gp.addProperty("grupaProizvoda", "naziv");		data.add(gp);	// grupa proizvoda
		
		GsonBuilder builder = new GsonBuilder();
		builder.excludeFieldsWithoutExposeAnnotation();
		builder.serializeNulls();
		Gson g = builder.create();
		
		renderJSON(data, g);
	}

	public static void addRow(String body) {
		System.out.println("-- add row --");
		Gson g = new Gson();
		models.akteri.Proizvod p = g.fromJson(body, models.akteri.Proizvod.class);
		
		p.merge();
		
		// Return data
		
		List<Object> data = new ArrayList<Object>();
		data.add(p);
		JsonObject pn = new JsonObject();		pn.addProperty("preduzece", "naziv");			data.add(pn);	// preduzece
		JsonObject jm = new JsonObject();		jm.addProperty("jedinicaMere", "naziv");		data.add(jm);	// jedinica mere
		JsonObject gp = new JsonObject();		gp.addProperty("grupaProizvoda", "naziv");		data.add(gp);	// grupa proizvoda
		
		GsonBuilder builder = new GsonBuilder();
		builder.excludeFieldsWithoutExposeAnnotation();
		builder.serializeNulls();
		Gson gr = builder.create();
		
		renderJSON(data, gr);
	}

	public static void updateRow(String body) {
		System.out.println("-- update row --");
		Gson g = new Gson();
		models.akteri.Proizvod p = g.fromJson(body, models.akteri.Proizvod.class);
		models.akteri.Proizvod po = models.akteri.Proizvod.findById(p.id);
		
		po.naziv = p.naziv;
		po.tip = p.tip;
		po.jedinicaMere = p.jedinicaMere;
		po.grupaProizvoda = p.grupaProizvoda;
		po.preduzece = p.preduzece;
		//po.proizvodJeStavkaCenovnika = p.proizvodJeStavkaCenovnika;
		
		po.save();
	}

	public static void deleteRow(int id) {
		models.akteri.Proizvod p = models.akteri.Proizvod.findById(id);
		
		if(p.proizvodJeStavkaCenovnika.isEmpty()) {
			p.delete();
			ok();
		}
		else {
			forbidden("Proizvod has child entities [stavke cenovnika]");
		}
	}

	public static void getNextTables() {
		Field[] fieldList = models.akteri.Proizvod.class.getFields();
		List<String> nextTables = new ArrayList<>();
		
		for(Field f: fieldList) {
			String name = f.getType().getName();
			if(name.contains("java.util.List")) {
				name = f.getGenericType().getTypeName();
				name = name.substring(name.lastIndexOf(".") + 1, name.lastIndexOf(">"));
				nextTables.add(name);
			}
		}
		
		renderJSON(nextTables);
	}
}
