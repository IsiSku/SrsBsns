package controllers;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import models.akteri.Cenovnik;
import models.akteri.PDV;
import models.akteri.Proizvod;
import play.mvc.Controller;

/**
 * Controller for {@link models.akteri.StavkaCenovnika}.
 * Manages CRUD and standard form actions.
 * 
 * @author Isidora Skulec
 */
public class StavkaCenovnika extends GenericController {
	
	public static void getAll() {
		List<models.akteri.StavkaCenovnika> all = models.akteri.StavkaCenovnika.findAll();

		List<Object> data = prepareData(all);
		
		GsonBuilder builder = new GsonBuilder();
		builder.excludeFieldsWithoutExposeAnnotation();
		builder.serializeNulls();
		Gson g = builder.create();
		
		renderJSON(data, g);
	}
	/**
	 * Returns all entities related to specified parent. Used for 'next' functionality
	 * @param parent Name of parent entity
	 * @param id Parent entity ID
	 * @author Lazar Anđelić
	 */
	public static void getByParent(String parent, Integer id) {
		String[] packages = {"models.akteri.", "models.dokumenti."};
		Object oId = null;
		if(parent.toLowerCase().equals("poslovnipartner")) {
			parent = "Preduzece";
		}
		for(String pack: packages) {
			try {
				Class c = Class.forName(pack + parent);
				oId = c.getMethod("findById", Object.class).invoke(null, id);
				break;
			} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException | LinkageError e) {
				continue;
			}
		}
		
		if(oId == null) {
			notFound("Invalid parent table");
			return;
		}

		String typeName = oId.getClass().getTypeName();
		typeName = typeName.substring(typeName.lastIndexOf(".") + 1);
		
		List<models.akteri.StavkaCenovnika> all = models.akteri.StavkaCenovnika.find("by" + typeName, oId).fetch();

		List<Object> data = prepareData(all);
		
		GsonBuilder builder = new GsonBuilder();
		builder.excludeFieldsWithoutExposeAnnotation();
		builder.serializeNulls();
		Gson g = builder.create();
		
		renderJSON(data, g);	
	}
	
	private static List<Object> prepareData(List<models.akteri.StavkaCenovnika> all) {
		if(all.isEmpty()) {
			models.akteri.StavkaCenovnika filler = new models.akteri.StavkaCenovnika();
			filler.id = null;
			filler.naziv = null;
			filler.cena = null;
			filler.cenovnik = new Cenovnik();
			filler.proizvod = new Proizvod();
			
			all.add(filler);
		}

		List<Object> data = new ArrayList<Object>();
		data.add(all);

		HashMap<String, String> fields = new HashMap<>();
		fields.put("cenovnik", "datumVazenja");
		fields.put("proizvod", "naziv");		
		data.add(fields);			

		HashMap<String, String> attrTypes = new HashMap<>();
		attrTypes.put("id", "number");
		attrTypes.put("naziv", "string");
		attrTypes.put("cena", "number");
		attrTypes.put("cenovnik", "string");
		attrTypes.put("proizvod", "string");
		data.add(attrTypes);
		
		return data;
	}
	
	public static void getByID(Integer id) {
		System.out.println("-- get by id --");
		models.akteri.StavkaCenovnika grupa = models.akteri.StavkaCenovnika.findById(id);
		
		List<Object> data = new ArrayList<Object>();
		data.add(grupa);
		JsonObject pn = new JsonObject();		pn.addProperty("proizvod", "naziv");			data.add(pn);	// proizvod
		JsonObject cn = new JsonObject();		cn.addProperty("cenovnik", "datumVazenja");		data.add(cn);	// cenovnik
		
		GsonBuilder builder = new GsonBuilder();
		builder.excludeFieldsWithoutExposeAnnotation();
		builder.serializeNulls();
		Gson g = builder.create();
		
		renderJSON(data, g);
	}
	
	public static void addRow(String body) {
		System.out.println("-- add row --");
		Gson g = new Gson();
		models.akteri.StavkaCenovnika p = g.fromJson(body, models.akteri.StavkaCenovnika.class);
		
		p.merge();
		
		// Return data
		
		Proizvod pro = Proizvod.findById(p.proizvod.id);
		p.proizvod = pro;
		Cenovnik cen = Cenovnik.findById(p.cenovnik.id);
		p.cenovnik = cen;
		
		
		List<Object> data = new ArrayList<Object>();
		data.add(p);
		JsonObject pn = new JsonObject();		pn.addProperty("proizvod", "naziv");			data.add(pn);	// proizvod
		JsonObject cn = new JsonObject();		cn.addProperty("cenovnik", "datumVazenja");		data.add(cn);	// cenovnik
		
		GsonBuilder builder = new GsonBuilder();
		builder.excludeFieldsWithoutExposeAnnotation();
		builder.serializeNulls();
		Gson gr = builder.create();
		
		renderJSON(data, gr);
	}
	
	public static void updateRow(String body) {
		System.out.println("-- update row --");
		Gson g = new Gson();
		models.akteri.StavkaCenovnika p = g.fromJson(body, models.akteri.StavkaCenovnika.class);
		models.akteri.StavkaCenovnika po = models.akteri.StavkaCenovnika.findById(p.id);
		
		po.naziv = p.naziv;
		po.cena = p.cena;
		po.cenovnik = p.cenovnik;
		po.proizvod = p.proizvod;
		
		po.save();
	}
	
	public static void deleteRow(Integer id) {
		models.akteri.StavkaCenovnika c = models.akteri.StavkaCenovnika.findById(id);
		c.delete();
		ok();
	}

	public static void getNextTables() {
		Field[] fieldList = models.akteri.StavkaCenovnika.class.getFields();
		List<String> nextTables = new ArrayList<>();
		
		for(Field f: fieldList) {
			String name = f.getType().getName();
			if(name.contains("java.util.List")) {
				name = f.getGenericType().getTypeName();
				name = name.substring(name.lastIndexOf(".") + 1, name.lastIndexOf(">"));
				nextTables.add(name);
			}
		}
		
		renderJSON(nextTables);
	}
}
