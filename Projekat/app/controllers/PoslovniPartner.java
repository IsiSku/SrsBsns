package controllers;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import models.akteri.Mesto;
import models.akteri.PDV;
import models.akteri.Preduzece;
import play.mvc.Controller;
import play.mvc.results.Result;

/**
 * Controller for business partners ({@link Preduzece}).
 * Manages CRUD and standard form actions.
 * 
 * @author Isidora Skulec
 */
public class PoslovniPartner extends GenericController {
	
	/**
	 * Reads all business partners from the database and returns their data.
	 * 
	 * @return {@link Result}
	 */
	public static void getAll() {
		System.out.println("-- get all --");
		
		List<Preduzece> partneri = Preduzece.find("byNasePreduzece", false).fetch();

		List<Object> data = prepareData(partneri);
		
		GsonBuilder builder = new GsonBuilder();
		builder.excludeFieldsWithoutExposeAnnotation();
		builder.serializeNulls();
		Gson g = builder.create();
		
		renderJSON(data, g);
	}
	/**
	 * Returns all entities related to specified parent. Used for 'next' functionality
	 * @param parent Name of parent entity
	 * @param id Parent entity ID
	 * @author Lazar Anđelić
	 */
	public static void getByParent(String parent, Integer id) {
		String[] packages = {"models.akteri.", "models.dokumenti."};
		Object oId = null;
		if(parent.toLowerCase().equals("poslovnipartner")) {
			parent = "Preduzece";
		}
		for(String pack: packages) {
			try {
				Class c = Class.forName(pack + parent);
				oId = c.getMethod("findById", Object.class).invoke(null, id);
				break;
			} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException | LinkageError e) {
				continue;
			}
		}
		
		if(oId == null) {
			notFound("Invalid parent table");
			return;
		}

		String typeName = oId.getClass().getTypeName();
		typeName = typeName.substring(typeName.lastIndexOf(".") + 1);
		
		List<models.akteri.Preduzece> all = models.akteri.Preduzece.find("by" + typeName, oId).fetch();
		
		List<Object> data = prepareData(all);
		
		GsonBuilder builder = new GsonBuilder();
		builder.excludeFieldsWithoutExposeAnnotation();
		builder.serializeNulls();
		Gson g = builder.create();
		
		renderJSON(data, g);	
	}
	
	private static List<Object> prepareData(List<models.akteri.Preduzece> partneri) {
		if(partneri.isEmpty()) {
			models.akteri.Preduzece filler = new models.akteri.Preduzece();
			filler.id = null;
			filler.naziv = null;
			filler.PIB = null;
			filler.maticniBroj = null;
			filler.ulica = null;
			filler.brojUlice = null;
			filler.brojTelefona = null;
			filler.email = null;
			filler.web = null;
			filler.nasePreduzece = null;
			filler.mesto = new Mesto();
			
			partneri.add(filler);
		}

		List<Object> data = new ArrayList<Object>();
		data.add(partneri);
		HashMap<String, String> fields = new HashMap<String, String>();
		fields.put("mesto", "naziv");
		data.add(fields);

		HashMap<String, String> attrTypes = new HashMap<>();
		attrTypes.put("id", "number");
		attrTypes.put("naziv", "string");
		attrTypes.put("PIB", "string");
		attrTypes.put("maticniBroj", "string");
		attrTypes.put("ulica", "string");
		attrTypes.put("brojUlice", "number");
		attrTypes.put("brojTelefona", "string");
		attrTypes.put("email", "string");
		attrTypes.put("web", "string");
		attrTypes.put("nasePreduzece", "boolean");
		attrTypes.put("mesto", "string");
		data.add(attrTypes);

		System.out.println(partneri.size());
		
		return data;
	}
	
	/**
	 * Finds the requested partner.
	 * 
	 * @param id
	 */
	public static void getByID(Integer id) {
		System.out.println("-- get by id --");
		Preduzece preduzece = Preduzece.findById(id);
		
		List<Object> data = new ArrayList<Object>();
		data.add(preduzece);
		JsonObject mn = new JsonObject();		mn.addProperty("mesto", "naziv");		data.add(mn);	// mesto
		
		GsonBuilder builder = new GsonBuilder();
		builder.excludeFieldsWithoutExposeAnnotation();
		builder.serializeNulls();
		Gson g = builder.create();
		
		renderJSON(data, g);
	}
	
	/**
	 * Inserts new entity into database.
	 * @param body
	 */
	public static void addRow(String body) {
		System.out.println("-- add row --");
		Gson g = new Gson();
		Preduzece p = g.fromJson(body, Preduzece.class);
		
		p.merge();
		
		// Return data
		
		Mesto m = Mesto.findById(p.mesto.id);
		p.mesto = m;
		
		List<Object> data = new ArrayList<Object>();
		data.add(p);
		JsonObject mn = new JsonObject();		mn.addProperty("mesto", "naziv");		data.add(mn);	// mesto
		
		GsonBuilder builder = new GsonBuilder();
		builder.excludeFieldsWithoutExposeAnnotation();
		builder.serializeNulls();
		Gson gr = builder.create();
		
		renderJSON(data, gr);
	}
	
	/**
	 * Updates existing entity with given object.
	 * 
	 * @param body
	 */
	public static void updateRow(String body) {
		System.out.println("-- update row --");
		Gson g = new Gson();
		Preduzece p = g.fromJson(body, Preduzece.class);
		Preduzece po = Preduzece.findById(p.id);
		
		po.naziv = p.naziv;
		po.PIB = p.PIB;
		po.maticniBroj = p.maticniBroj;
		po.ulica = p.ulica;
		po.brojUlice = p.brojUlice;
		po.brojTelefona = p.brojTelefona;
		po.email = p.email;
		po.web = p.web;
		po.nasePreduzece = p.nasePreduzece;
		//po.proizvodList = p.proizvodList;
		//po.cenovnikList = p.cenovnikList;
		//po.ListaNasihPoslovnihPartnera = p.ListaNasihPoslovnihPartnera;
		//po.ListaKomeSmoMiPoslovniPartneri = p.ListaKomeSmoMiPoslovniPartneri;
		po.mesto = p.mesto;
		//po.poslovnaGodinaList = p.poslovnaGodinaList;
		po.save();		
	}
	
	/**
	 * Deletes the specified entity.
	 * @param id
	 */
	public static void deleteRow(Integer id) {
		Preduzece p = Preduzece.findById(id);
		
		if(p.proizvodList.isEmpty()) {
			if(p.cenovnikList.isEmpty()) {
				if(p.ListaNasihPoslovnihPartnera.isEmpty() && p.ListaKomeSmoMiPoslovniPartneri.isEmpty()) {
					p.delete();
					ok();
				}
				else {
					forbidden("Preduzece (partner) has child entities [poslovni partneri]");
				}
			}
			else {
				forbidden("Preduzece (partner) has child entities [cenovnici]");
			}
		}
		else {
			forbidden("Preduzece (partner) has child entities [proizvodi]");
		}
		
		p.delete();
	}

	public static void getNextTables() {
		//TODO da li vratiti podatke za Preduzece ili Poslovne Partnere?
		Field[] fieldList = models.akteri.Preduzece.class.getFields();
		List<String> nextTables = new ArrayList<>();
		
		for(Field f: fieldList) {
			String name = f.getType().getName();
			if(name.contains("java.util.List")) {
				name = f.getGenericType().getTypeName();
				name = name.substring(name.lastIndexOf(".") + 1, name.lastIndexOf(">"));
				nextTables.add(name);
			}
		}
		
		renderJSON(nextTables);
	}
}
