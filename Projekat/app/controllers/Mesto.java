package controllers;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import models.akteri.PDV;
import models.akteri.Preduzece;

/**
 * Controller for {@link models.akteri.Mesto}.
 * Manages CRUD and standard form actions.
 * 
 * @author Isidora Skulec
 */
public class Mesto extends GenericController {
	
	public static void getAll() {
		List<models.akteri.Mesto> all = models.akteri.Mesto.findAll();

		List<Object> data = prepareData(all);
		
		GsonBuilder builder = new GsonBuilder();
		builder.excludeFieldsWithoutExposeAnnotation();
		builder.serializeNulls();
		Gson g = builder.create();
		
		renderJSON(data, g);
	}
	/**
	 * Returns all entities related to specified parent. Used for 'next' functionality
	 * @param parent Name of parent entity
	 * @param id Parent entity ID
	 * @author Lazar Anđelić
	 */
	public static void getByParent(String parent, Integer id) {
		String[] packages = {"models.akteri.", "models.dokumenti."};
		Object oId = null;
		if(parent.toLowerCase().equals("poslovnipartner")) {
			parent = "Preduzece";
		}
		for(String pack: packages) {
			try {
				Class c = Class.forName(pack + parent);
				oId = c.getMethod("findById", Object.class).invoke(null, id);
				break;
			} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException | LinkageError e) {
				continue;
			}
		}
		
		if(oId == null) {
			notFound("Invalid parent table");
			return;
		}

		String typeName = oId.getClass().getTypeName();
		typeName = typeName.substring(typeName.lastIndexOf(".") + 1);
		
		List<models.akteri.Mesto> all = models.akteri.Mesto.find("by" + typeName, oId).fetch();
		
		List<Object> data = prepareData(all);
		
		GsonBuilder builder = new GsonBuilder();
		builder.excludeFieldsWithoutExposeAnnotation();
		builder.serializeNulls();
		Gson g = builder.create();
		
		renderJSON(data, g);	
	}
	
	private static List<Object> prepareData(List<models.akteri.Mesto> all) {
		if(all.isEmpty()) {
			models.akteri.Mesto filler = new models.akteri.Mesto();
			filler.id = null;
			filler.naziv = null;
			filler.postanskiBroj = null;
			
			all.add(filler);
		}

		List<Object> data = new ArrayList<Object>();
		data.add(all);
		
		HashMap<String, String> fields = new HashMap<String, String>();
		data.add(fields);
		
		HashMap<String, String> attrTypes = new HashMap<>();
		attrTypes.put("id", "number");
		attrTypes.put("naziv", "string");
		attrTypes.put("postanskiBroj", "number");		
		data.add(attrTypes);
		
		return data;
	}
	
	public static void getByID(Integer id) {
		System.out.println("-- get by id --");
		models.akteri.Mesto grupa = models.akteri.Mesto.findById(id);
		
		List<Object> data = new ArrayList<Object>();
		data.add(grupa);
		
		GsonBuilder builder = new GsonBuilder();
		builder.excludeFieldsWithoutExposeAnnotation();
		builder.serializeNulls();
		Gson g = builder.create();
		
		renderJSON(data, g);
	}
	
	public static void addRow(String body) {
		System.out.println("-- add row --");
		Gson g = new Gson();
		models.akteri.Mesto p = g.fromJson(body, models.akteri.Mesto.class);
		
		p.merge();
		
		// Return data
		
		List<Object> data = new ArrayList<Object>();
		data.add(p);
		
		GsonBuilder builder = new GsonBuilder();
		builder.excludeFieldsWithoutExposeAnnotation();
		builder.serializeNulls();
		Gson gr = builder.create();
		
		renderJSON(data, gr);

	}
	
	public static void updateRow(String body) {
		System.out.println("-- update row --");
		Gson g = new Gson();
		models.akteri.Mesto p = g.fromJson(body, models.akteri.Mesto.class);
		models.akteri.Mesto po = models.akteri.Mesto.findById(p.id);
		
		po.postanskiBroj = p.postanskiBroj;
		po.naziv = p.naziv;
		
		po.save();
	}
	
	public static void deleteRow(Integer id) {
		models.akteri.Mesto c = models.akteri.Mesto.findById(id);
		
		List<Preduzece> gp = c.preduzeca;
		if(gp.isEmpty()) {
			c.delete();
			ok();
		}
		else {
			forbidden("Mesto has child entities [preduzeca].");
		}
	}

	public static void getNextTables() {
		Field[] fieldList = models.akteri.Mesto.class.getFields();
		List<String> nextTables = new ArrayList<>();
		
		for(Field f: fieldList) {
			String name = f.getType().getName();
			if(name.contains("java.util.List")) {
				name = f.getGenericType().getTypeName();
				name = name.substring(name.lastIndexOf(".") + 1, name.lastIndexOf(">"));
				nextTables.add(name);
			}
		}
		
		renderJSON(nextTables);
	}
}
