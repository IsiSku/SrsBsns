package controllers;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import models.akteri.GrupaProizvoda;
import models.akteri.StopaPDVa;

/**
 * Controller for {@link models.akteri.PDV}.
 * Manages CRUD and standard form actions.
 * 
 * @author Isidora Skulec
 */
public class PDV extends GenericController {
	
	public static void getAll() {
		List<models.akteri.PDV> all = models.akteri.PDV.findAll();

		List<Object> data = prepareData(all);
		
		GsonBuilder builder = new GsonBuilder();
		builder.excludeFieldsWithoutExposeAnnotation();
		builder.serializeNulls();
		Gson g = builder.create();
		
		renderJSON(data, g);
	}
	/**
	 * Returns all entities related to specified parent. Used for 'next' functionality
	 * @param parent Name of parent entity
	 * @param id Parent entity ID
	 * @author Lazar Anđelić
	 */
	public static void getByParent(String parent, Integer id) {
		String[] packages = {"models.akteri.", "models.dokumenti."};
		Object oId = null;
		if(parent.toLowerCase().equals("poslovnipartner")) {
			parent = "Preduzece";
		}
		for(String pack: packages) {
			try {
				Class c = Class.forName(pack + parent);
				oId = c.getMethod("findById", Object.class).invoke(null, id);
				break;
			} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException | LinkageError e) {
				continue;
			}
		}
		
		if(oId == null) {
			notFound("Invalid parent table");
			return;
		}

		String typeName = oId.getClass().getTypeName();
		typeName = typeName.substring(typeName.lastIndexOf(".") + 1);
		
		List<models.akteri.PDV> all = models.akteri.PDV.find("by" + typeName, oId).fetch();
		
		List<Object> data = prepareData(all);
		
		GsonBuilder builder = new GsonBuilder();
		builder.excludeFieldsWithoutExposeAnnotation();
		builder.serializeNulls();
		Gson g = builder.create();
		
		renderJSON(data, g);	
	}
	
	private static List<Object> prepareData(List<models.akteri.PDV> all) {
		if(all.isEmpty()) {
			models.akteri.PDV filler = new models.akteri.PDV();
			filler.id = null;
			filler.naziv = null;
			
			all.add(filler);
		}

		List<Object> data = new ArrayList<Object>();
		data.add(all);
		
		HashMap<String, String> fields = new HashMap<String, String>();
		data.add(fields); // prazno jer nema foreign key-ova
		
		HashMap<String, String> attrTypes = new HashMap<>();
		attrTypes.put("id", "number");		// tip za ID
		attrTypes.put("naziv", "string");	// tip za naziv
		data.add(attrTypes);
		
		return data;
	}
	
	public static void getByID(Integer id) {
		System.out.println("-- get by id --");
		models.akteri.PDV grupa = models.akteri.PDV.findById(id);
		
		List<Object> data = new ArrayList<Object>();
		data.add(grupa);
		
		GsonBuilder builder = new GsonBuilder();
		builder.excludeFieldsWithoutExposeAnnotation();
		builder.serializeNulls();
		Gson g = builder.create();
		
		renderJSON(data, g);
	}
	
	public static void addRow(String body) {
		System.out.println("-- add row --");
		Gson g = new Gson();
		models.akteri.PDV p = g.fromJson(body, models.akteri.PDV.class);
		
		p.merge();
		
		// Return data
		
		List<Object> data = new ArrayList<Object>();
		data.add(p);
		
		GsonBuilder builder = new GsonBuilder();
		builder.excludeFieldsWithoutExposeAnnotation();
		builder.serializeNulls();
		Gson gr = builder.create();
		
		renderJSON(data, gr);

	}
	
	public static void updateRow(String body) {
		System.out.println("-- update row --");
		Gson g = new Gson();
		models.akteri.PDV p = g.fromJson(body, models.akteri.PDV.class);
		models.akteri.PDV po = models.akteri.PDV.findById(p.id);
		
		po.naziv = p.naziv;
		
		po.save();
	}
	
	public static void deleteRow(Integer id) {
		models.akteri.PDV c = models.akteri.PDV.findById(id);
		
		List<GrupaProizvoda> gp = c.grupe;
		List<StopaPDVa> sp = c.stope;
		if(gp.isEmpty()) {
			if(sp.isEmpty()) {
				c.delete();
				ok();
			}
			forbidden("PDV has child entities [stope].");
		}
		else {
			forbidden("PDV has child entities [grupe].");
		}
	}

	public static void getNextTables() {
		Field[] fieldList = models.akteri.PDV.class.getFields();
		List<String> nextTables = new ArrayList<>();
		
		for(Field f: fieldList) {
			String name = f.getType().getName();
			if(name.contains("java.util.List")) {
				name = f.getGenericType().getTypeName();
				name = name.substring(name.lastIndexOf(".") + 1, name.lastIndexOf(">"));
				nextTables.add(name);
			}
		}
		
		renderJSON(nextTables);
	}
}
