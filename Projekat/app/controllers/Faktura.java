package controllers;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import models.akteri.PoslovnaGodina;
import models.akteri.Preduzece;
import models.dokumenti.Narudzbenica;

/**
 * 
 * @author Boki
 *
 */

public class Faktura extends GenericController {
	
	public static void getAll(){
		System.out.println("sve fakture");
		
		List<models.dokumenti.Faktura> fakture = models.dokumenti.Faktura.findAll();
		List<Object> data = prepareData(fakture);
		
		GsonBuilder builder = new GsonBuilder();
		builder.excludeFieldsWithoutExposeAnnotation();
		builder.serializeNulls();
		Gson g = builder.create();		

		renderJSON(data, g);
	}
	/**
	 * Returns all entities related to specified parent. Used for 'next' functionality
	 * @param parent Name of parent entity
	 * @param id Parent entity ID
	 * @author Lazar Anđelić
	 */
	public static void getByParent(String parent, Integer id) {
		String[] packages = {"models.akteri.", "models.dokumenti."};
		Object oId = null;
		if(parent.toLowerCase().equals("poslovnipartner") || parent.toLowerCase().contains("koga")) {
			parent = "Preduzece";
		}
		for(String pack: packages) {
			try {
				Class c = Class.forName(pack + parent);
				oId = c.getMethod("findById", Object.class).invoke(null, id);
				break;
			} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException | LinkageError e) {
				continue;
			}
		}
		
		if(oId == null) {
			notFound("Invalid parent table");
			return;
		}

		String typeName = oId.getClass().getTypeName();
		typeName = typeName.substring(typeName.lastIndexOf(".") + 1);
		
		List<models.dokumenti.Faktura> all = models.dokumenti.Faktura.find("by" + typeName, oId).fetch();
		
		List<Object> data = prepareData(all);
		
		GsonBuilder builder = new GsonBuilder();
		builder.excludeFieldsWithoutExposeAnnotation();
		builder.serializeNulls();
		Gson g = builder.create();
		
		renderJSON(data, g);	
	}
	
	private static List<Object> prepareData(List<models.dokumenti.Faktura> fakture) {
		if(fakture.isEmpty()) {
			models.dokumenti.Faktura filler = new models.dokumenti.Faktura();
			filler.id = null;
			filler.broj = null;
			filler.datum = null;
			filler.datumValute = null;
			filler.ukupanRabat = null;
			filler.ukupanIznosBezPDVa = null;
			filler.ukupanIznosPDVa = null;
			filler.ukupanIznosZaPlacanje = null;
			filler.smer = null;
			filler.napomena = null;
			filler.prevoznik = null;
			filler.brVozila = null;
			filler.vozac = null;
			filler.odgovornoLice = null;
			filler.primioRobu = null;
			filler.narudzbenica = new Narudzbenica();
			filler.poslovnaGodina = new PoslovnaGodina();
			filler.odKoga = new Preduzece();
			filler.zaKoga = new Preduzece();
			
			fakture.add(filler);
		}

		List<Object> data = new ArrayList<Object>();
		data.add(fakture);

		HashMap<String, String> fields = new HashMap<>();
		fields.put("narudzbenica", "broj");
		fields.put("poslovnaGodina", "godina");
		fields.put("odKoga", "naziv");
		fields.put("zaKoga", "naziv");
		data.add(fields);
		
		HashMap<String, String> attrTypes = new HashMap<>();
		attrTypes.put("id", "number");
		attrTypes.put("broj", "number");
		attrTypes.put("datum", "string");
		attrTypes.put("datumValute", "string");
		attrTypes.put("ukupanRabat", "number");
		attrTypes.put("ukupanIznosBezPDVa", "number");
		attrTypes.put("ukupanIznosPDVa", "number");
		attrTypes.put("ukupanIznosZaPlacanje", "number");
		attrTypes.put("smer", "string");
		attrTypes.put("napomena", "string");
		attrTypes.put("prevoznik", "string");
		attrTypes.put("brVozila", "string");
		attrTypes.put("vozac", "string");
		attrTypes.put("odgovornoLice", "string");
		attrTypes.put("primioRobu", "string");
		attrTypes.put("narudzbenica", "number");
		attrTypes.put("poslovnaGodina", "string");
		attrTypes.put("odKoga", "string");
		attrTypes.put("zaKoga", "string");
		data.add(attrTypes);
		
		HashMap<String, Boolean> visFields = new HashMap<String, Boolean>(); //polja koja se automatski izracunavaju, ne unose se rucno
		
		visFields.put("id", false);
		visFields.put("ukupanRabat", false);
		visFields.put("ukupanIznosBezPDVa", false);
		visFields.put("ukupanIznosPDVa", false);
		visFields.put("ukupanIznosZaPlacanje", false);
		visFields.put("poslovnaGodina", false);
		data.add(visFields);
		
		return data;
	}

	public static void getByID(Integer id) {
		System.out.println("-- get by id --");
		models.dokumenti.Faktura fakture = models.dokumenti.Faktura.findById(id);
		
		List<Object> data = new ArrayList<Object>();
		data.add(fakture);
		
		JsonObject jog = new JsonObject();
		jog.addProperty("godina", "godina");
		data.add(jog);
		JsonObject joo = new JsonObject();
		joo.addProperty("odKoga", "naziv");
		data.add(joo);
		JsonObject joz = new JsonObject();
		joz.addProperty("zaKoga", "naziv");
		data.add(joz);
		
		GsonBuilder builder = new GsonBuilder();
		builder.excludeFieldsWithoutExposeAnnotation();
		builder.serializeNulls();
		Gson g = builder.create();
		
		renderJSON(data, g);
	}	
	
	public static void addRow(String body) {
		System.out.println("dodata nova faktura");
		
		Gson g = new Gson();
		models.dokumenti.Faktura f = g.fromJson(body, models.dokumenti.Faktura.class);
		f.merge();
		
		PoslovnaGodina god = PoslovnaGodina.findById(f.poslovnaGodina.id);
		Preduzece od = Preduzece.findById(f.odKoga.id);
		Preduzece za = Preduzece.findById(f.zaKoga.id);
		f.poslovnaGodina = god;
		f.odKoga = od;
		f.zaKoga = za;
		
		List<Object> data = new ArrayList<Object>();
		data.add(f);
		
		JsonObject jog = new JsonObject();
		jog.addProperty("godina", "godina");
		data.add(jog);
		JsonObject joo = new JsonObject();
		joo.addProperty("odKoga", "naziv");
		data.add(joo);
		JsonObject joz = new JsonObject();
		joz.addProperty("zaKoga", "naziv");
		data.add(joz);		
		
		GsonBuilder builder = new GsonBuilder();
		builder.excludeFieldsWithoutExposeAnnotation();
		builder.serializeNulls();
		Gson gb = builder.create();		
		
		renderJSON(data, gb);
	}
	
	public static void updateRow(String body) {
		System.out.println("update row");
		
		Gson g = new Gson();
		
		models.dokumenti.Faktura f = g.fromJson(body, models.dokumenti.Faktura.class);
		models.dokumenti.Faktura f1 = models.dokumenti.Faktura.findById(f.id);
		
		f1.broj = f.broj;
		f1.datum = f.datum;
		f1.datumValute = f.datumValute;
		f1.ukupanRabat = f.ukupanRabat;
		f1.ukupanIznosBezPDVa = f.ukupanIznosBezPDVa;
		f1.ukupanIznosPDVa = f.ukupanIznosPDVa;
		f1.ukupanIznosZaPlacanje = f.ukupanIznosZaPlacanje;
		f1.smer = f.smer;
		f1.napomena = f.napomena;
		f1.prevoznik = f.prevoznik;
		f1.brVozila = f.brVozila;
		f1.vozac = f.vozac;
		f1.odgovornoLice = f.odgovornoLice;
		f1.primioRobu = f.primioRobu;
		f1.stavke = f.stavke;
		f1.narudzbenica = f.narudzbenica;
		f1.poslovnaGodina = f.poslovnaGodina;
		f1.odKoga = f.odKoga;
		f1.zaKoga = f.zaKoga;
		
		f1.save();
	}
	
	public static void deleteRow(Integer id) {
		models.dokumenti.Faktura f = models.dokumenti.Faktura.findById(id);
		f.delete();
	}
	
	public static void getNextTables() {
		Field[] fieldList = models.dokumenti.Faktura.class.getFields();
		List<String> nextTables = new ArrayList<>();
		
		for(Field f: fieldList) {
			String name = f.getType().getName();
			if(name.contains("java.util.List")) {
				name = f.getGenericType().getTypeName();
				name = name.substring(name.lastIndexOf(".") + 1, name.lastIndexOf(">"));
				nextTables.add(name);
			}
		}
		
		renderJSON(nextTables);
	}
}
