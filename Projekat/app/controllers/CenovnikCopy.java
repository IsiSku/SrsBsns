package controllers;

import models.akteri.Cenovnik;
import models.akteri.StavkaCenovnika;
import play.mvc.Controller;
import play.mvc.results.Error;
import play.mvc.results.Ok;
import play.mvc.results.Result;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Nemanja on 24/6/2016.
 */
public class CenovnikCopy extends Controller {

	public static Result copyAndAddPercentageToEntireCenovnik() {
		Integer stariCenovnikId = params.get("id", Integer.class);
		double procenat = params.get("percent", double.class);

		String datumString = params.get("date", String.class);
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");  //ovako je podešeno u conf fajlu
		Date noviDatum;
		try {
			noviDatum = sdf.parse(datumString);
		} catch (ParseException e) {
			e.printStackTrace();
			return new Error("Unable to create date");
		}

		models.akteri.Cenovnik stariCenovnik = Cenovnik.findById(stariCenovnikId);

		Cenovnik noviCenovnik = new Cenovnik();

		noviCenovnik.preduzece = stariCenovnik.preduzece;
		noviCenovnik.datumVazenja = noviDatum;

		noviCenovnik.save();    //moramo prvo da napravimo cenovnik da bismo mogli da dodajemo stavke u njega

		for (StavkaCenovnika staraStavkaCenovnika : stariCenovnik.stavkeCenovnika){
			double novaCena = staraStavkaCenovnika.cena + staraStavkaCenovnika.cena*(procenat/100.0);

			StavkaCenovnika novaStavkaCenovnika = new StavkaCenovnika(staraStavkaCenovnika.naziv,
					novaCena, noviCenovnik, staraStavkaCenovnika.proizvod);

			novaStavkaCenovnika.save();
		}

		return new Ok();
	}

	public static Result copyCenovnikAndEditGroup() {
		Integer groupId = params.get("groupId", Integer.class);
		Integer stariCenovnikId = params.get("cenovnikId", Integer.class);
		double percent = params.get("percent", double.class);

		String datumString = params.get("date", String.class);
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");  //ovako je podešeno u conf fajlu
		Date noviDatum;
		try {
			noviDatum = sdf.parse(datumString);
		} catch (ParseException e) {
			e.printStackTrace();
			return new Error("Unable to create date");
		}

		Cenovnik stariCenovnik = Cenovnik.findById(stariCenovnikId);

		Cenovnik noviCenovnik = new Cenovnik();

		noviCenovnik.preduzece = stariCenovnik.preduzece;
		noviCenovnik.datumVazenja = noviDatum;

		noviCenovnik.save();

		for (StavkaCenovnika staraStavkaCenovnika : stariCenovnik.stavkeCenovnika) {
			if (staraStavkaCenovnika.proizvod.grupaProizvoda.id == groupId) {
				double novaCena = staraStavkaCenovnika.cena + staraStavkaCenovnika.cena*(percent/100.0);

				StavkaCenovnika novaStavkaCenovnika = new StavkaCenovnika(staraStavkaCenovnika.naziv,
						(double) novaCena, noviCenovnik, staraStavkaCenovnika.proizvod);

				novaStavkaCenovnika.save();
			} else {
				StavkaCenovnika novaStavkaCenovnika = new StavkaCenovnika(staraStavkaCenovnika.naziv,
						staraStavkaCenovnika.cena, noviCenovnik, staraStavkaCenovnika.proizvod);

				novaStavkaCenovnika.save();
			}
		}

		return new Ok();
	}

	public static Result copyCenovnikAndEditItem() {
		Integer stavkaCenovnikaId = params.get("stavkaCenovnikaId", Integer.class);
		Integer stariCenovnikId = params.get("stariCenovnikId", Integer.class);
		double percent = params.get("percent", double.class);

		String datumString = params.get("date", String.class);
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");  //ovako je podešeno u conf fajlu
		Date noviDatum;
		try {
			noviDatum = sdf.parse(datumString);
		} catch (ParseException e) {
			e.printStackTrace();
			return new Error("Unable to create date");
		}


		Cenovnik stariCenovnik = Cenovnik.findById(stariCenovnikId);

		Cenovnik noviCenovnik = new Cenovnik();

		noviCenovnik.preduzece = stariCenovnik.preduzece;
		noviCenovnik.datumVazenja = noviDatum;

		noviCenovnik.save();

		for (StavkaCenovnika staraStavkaCenovnika : stariCenovnik.stavkeCenovnika) {
			if (staraStavkaCenovnika.id == stavkaCenovnikaId) {
				double novaCena = staraStavkaCenovnika.cena + staraStavkaCenovnika.cena*(percent/100.0);

				StavkaCenovnika novaStavkaCenovnika = new StavkaCenovnika(staraStavkaCenovnika.naziv,
						(double) novaCena, noviCenovnik, staraStavkaCenovnika.proizvod);

				novaStavkaCenovnika.save();
			} else {
				StavkaCenovnika novaStavkaCenovnika = new StavkaCenovnika(staraStavkaCenovnika.naziv,
						staraStavkaCenovnika.cena, noviCenovnik, staraStavkaCenovnika.proizvod);

				novaStavkaCenovnika.save();
			}
		}

		return new Ok();
	}
}
