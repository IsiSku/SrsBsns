package controllers;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import models.akteri.Cenovnik;
import models.akteri.GrupaProizvoda;
import models.akteri.JedinicaMere;
import models.akteri.PDV;
import models.akteri.Proizvod;
import models.akteri.StavkaCenovnika;
import models.dokumenti.Faktura;
import models.dokumenti.Narudzbenica;
import play.db.DB;

public class StavkaNarudzbenice extends GenericController{

	private static String PDV_STATEMENT = "SELECT id, max(datum), procenat, pdv FROM StopePDVa WHERE pdv=? GROUP BY id, procenat, pdv";
	private static String CENOVNIK_STATEMENT = "SELECT id, max(datumVazenja) FROM Cenovnik";
	
	private static double getStopaPDVa(Integer prID){
		Double stopa = null;
		Proizvod proizvod = Proizvod.findById(prID);
		GrupaProizvoda grupaProizvoda = proizvod.grupaProizvoda;
		PDV pdv = grupaProizvoda.pdv;
		
		PreparedStatement ps;
		try {
			ps = DB.getConnection().prepareStatement(PDV_STATEMENT);
			ps.setInt(1, pdv.id);
			ResultSet resPDV = ps.executeQuery();
			resPDV.next();
			
			Integer stopaID = resPDV.getInt(1);
			Date stopaDatum = resPDV.getDate(2);
			Double stopaProcenat = resPDV.getDouble(3);
			stopa = stopaProcenat;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return stopa;
	}
	
	private static double getPrice(Integer prID) {
		double cena = 0;
		
		//PDV
		Proizvod proizvod = Proizvod.findById(prID);
//		GrupaProizvoda grupaProizvoda = proizvod.grupaProizvoda;
//		PDV pdv = grupaProizvoda.pdv;
		
		//CENA
		
		
		try {
			// StopaPDVa
//			PreparedStatement ps = DB.getConnection().prepareStatement(PDV_STATEMENT);
//			ps.setInt(1, pdv.id);
//			ResultSet resPDV = ps.executeQuery();
//			resPDV.next();
//			
//			Integer stopaID = resPDV.getInt(1);
//			Date stopaDatum = resPDV.getDate(2);
//			Double stopaProcenat = resPDV.getDouble(3);
			
			// Cenovnik
			PreparedStatement cs = DB.getConnection().prepareStatement(CENOVNIK_STATEMENT);
			ResultSet resCenovnik = cs.executeQuery();
			resCenovnik.next();
			
			Integer cenovnikID = resCenovnik.getInt(1);
			Date cenovnikDatum = resCenovnik.getDate(2);
			Cenovnik cenovnik = Cenovnik.findById(cenovnikID);
			
			StavkaCenovnika stavkaCenovnika = (StavkaCenovnika) StavkaCenovnika.find("byCenovnikAndProizvod", cenovnik, proizvod).fetch().get(0);
			cena = stavkaCenovnika.cena;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return cena;
	}

	
	public static void getAll() {
		List<models.dokumenti.StavkaNarudzbenice> all = models.dokumenti.StavkaNarudzbenice.findAll();

		List<Object> data = prepareData(all);
		
		GsonBuilder builder = new GsonBuilder();
		builder.excludeFieldsWithoutExposeAnnotation();
		builder.serializeNulls();
		Gson g = builder.create();
		
		renderJSON(data, g);	
	}
	/**
	 * Returns all entities related to specified parent. Used for 'next' functionality
	 * @param parent Name of parent entity
	 * @param id Parent entity ID
	 * @author Lazar Anđelić
	 */
	public static void getByParent(String parent, Integer id) {
		String[] packages = {"models.akteri.", "models.dokumenti."};
		Object oId = null;
		if(parent.toLowerCase().equals("poslovnipartner")) {
			parent = "Preduzece";
		}
		for(String pack: packages) {
			try {
				Class c = Class.forName(pack + parent);
				oId = c.getMethod("findById", Object.class).invoke(null, id);
				break;
			} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException | LinkageError e) {
				continue;
			}
		}
		
		if(oId == null) {
			notFound("Invalid parent table");
			return;
		}

		String typeName = oId.getClass().getTypeName();
		typeName = typeName.substring(typeName.lastIndexOf(".") + 1);
		
		List<models.dokumenti.StavkaNarudzbenice> all = models.dokumenti.StavkaNarudzbenice.find("by" + typeName, oId).fetch();
		
		List<Object> data = prepareData(all);
		
		GsonBuilder builder = new GsonBuilder();
		builder.excludeFieldsWithoutExposeAnnotation();
		builder.serializeNulls();
		Gson g = builder.create();
		
		renderJSON(data, g);	
	}
	
	private static List<Object> prepareData(List<models.dokumenti.StavkaNarudzbenice> all) {
		if(all.isEmpty()) {
			models.dokumenti.StavkaNarudzbenice	filler = new models.dokumenti.StavkaNarudzbenice();
			filler.id = null;
			filler.jedinicaMere = new JedinicaMere();
			filler.kolicina = null;
			filler.narudzbenica = new Narudzbenica();
			filler.proizvod = new Proizvod();
			
			all.add(filler);
		}

		List<Object> data = new ArrayList<Object>();
		data.add(all);

		HashMap<String, String> fields = new HashMap<>();
		fields.put("jedinicaMere", "naziv");
		fields.put("narudzbenica", "broj");
		fields.put("proizvod", "naziv");		
		data.add(fields);
		
		HashMap<String, String> attrTypes = new HashMap<>();
		attrTypes.put("id", "number");
		attrTypes.put("kolicina", "number");
		attrTypes.put("jedinicaMere", "string");
		attrTypes.put("narudzbenica", "number");
		attrTypes.put("proizvod", "string");		
		data.add(attrTypes);
		
		return data;
	}
	
	public static void getByID(Integer id) {
		System.out.println("-- get by id --");
		models.dokumenti.StavkaNarudzbenice grupa = models.dokumenti.StavkaNarudzbenice.findById(id);
		
		List<Object> data = new ArrayList<Object>();
		data.add(grupa);
		JsonObject jof = new JsonObject();
		jof.addProperty("narudzbenica", "broj");
		data.add(jof);
		JsonObject jop = new JsonObject();
		jop.addProperty("proizvod", "naziv");
		data.add(jop);

		GsonBuilder builder = new GsonBuilder();
		builder.excludeFieldsWithoutExposeAnnotation();
		builder.serializeNulls();
		Gson g = builder.create();
		
		renderJSON(data, g);
	}
	
	public static void addRow(String body) {
		System.out.println("-- add row --");
		Gson g = new Gson();
		models.dokumenti.StavkaNarudzbenice sn = g.fromJson(body, models.dokumenti.StavkaNarudzbenice.class);
		
		sn.merge();
		
		// Return data
		Narudzbenica nar = Narudzbenica.findById(sn.narudzbenica.id);
		sn.narudzbenica = nar;
		
		Proizvod pro = Proizvod.findById(sn.proizvod.id);
		sn.proizvod = pro;
		
		
		List<Object> data = new ArrayList<Object>();
		data.add(sn);
		JsonObject jof = new JsonObject();
		jof.addProperty("narudzbenica", "broj");
		data.add(jof);
		JsonObject jop = new JsonObject();
		jop.addProperty("proizvod", "naziv");
		data.add(jop);

		GsonBuilder builder = new GsonBuilder();
		builder.excludeFieldsWithoutExposeAnnotation();
		builder.serializeNulls();
		Gson gr = builder.create();
		
		renderJSON(data, gr);
	}
	
	public static void updateRow(String body) {
		System.out.println("-- update row --");
		Gson g = new Gson();
		models.dokumenti.StavkaNarudzbenice n = g.fromJson(body, models.dokumenti.StavkaNarudzbenice.class);
		models.dokumenti.StavkaNarudzbenice n1 = models.dokumenti.StavkaNarudzbenice.findById(n.id);
		
		n1.jedinicaMere = n.jedinicaMere;
		n1.kolicina = n.kolicina;
		n1.narudzbenica = n.narudzbenica;
		n1.proizvod = n.proizvod;
		n1.save();
	}
	
	public static void deleteRow(Integer id) {
		models.dokumenti.StavkaNarudzbenice n = models.dokumenti.StavkaNarudzbenice.findById(id);
		n.delete();
		ok();
	}
	
	public static void getNextTables() {
		Field[] fieldList = models.dokumenti.StavkaNarudzbenice.class.getFields();
		List<String> nextTables = new ArrayList<>();
		
		for(Field f: fieldList) {
			String name = f.getType().getName();
			if(name.contains("java.util.List")) {
				name = f.getGenericType().getTypeName();
				name = name.substring(name.lastIndexOf(".") + 1, name.lastIndexOf(">"));
				nextTables.add(name);
			}
		}
		
		renderJSON(nextTables);
	}
}
