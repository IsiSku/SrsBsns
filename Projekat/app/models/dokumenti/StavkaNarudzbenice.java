package models.dokumenti;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.google.gson.annotations.Expose;

import models.akteri.Cenovnik;
import models.akteri.GrupaProizvoda;
import models.akteri.JedinicaMere;
import models.akteri.PDV;
import models.akteri.Proizvod;
import models.akteri.StavkaCenovnika;
import play.db.DB;
import play.db.jpa.GenericModel;

@Entity(name="StavkeNarudzbenice")
public class StavkaNarudzbenice extends GenericModel{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	@Expose
	public Integer id;

	@Column(length = 10)
	@Expose
	public JedinicaMere jedinicaMere;
	
	@Column(length = 10)
	@Expose
	public Double kolicina;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "narudzbenica", referencedColumnName = "id", nullable = false)
	@Expose
	public Narudzbenica narudzbenica;
	
//	TODO: preuzeti iz aktera
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "proizvod", referencedColumnName = "id", nullable = false)
	@Expose
	public Proizvod proizvod;

	public StavkaNarudzbenice() {
		super();
	}

	public StavkaNarudzbenice(JedinicaMere jedMere, Double kolicina, Narudzbenica narudzbenica, Proizvod proizvod) {
		super();
		this.jedinicaMere = jedMere;
		this.kolicina = kolicina;
		this.narudzbenica = narudzbenica;
		this.proizvod = proizvod;
	}
	
	private static String PDV_STATEMENT = "SELECT id, max(datum), procenat, pdv FROM StopePDVa WHERE pdv=? GROUP BY id, procenat, pdv";
	private static String CENOVNIK_STATEMENT = "SELECT id, max(datumVazenja) FROM Cenovnik";
	
	public static double getStopaPDVa(Integer prID){
		Double stopa = null;
		Proizvod proizvod = Proizvod.findById(prID);
		GrupaProizvoda grupaProizvoda = proizvod.grupaProizvoda;
		PDV pdv = grupaProizvoda.pdv;
		
		PreparedStatement ps;
		try {
			ps = DB.getConnection().prepareStatement(PDV_STATEMENT);
			ps.setInt(1, pdv.id);
			ResultSet resPDV = ps.executeQuery();
			resPDV.next();
			
			Integer stopaID = resPDV.getInt(1);
			Date stopaDatum = resPDV.getDate(2);
			Double stopaProcenat = resPDV.getDouble(3);
			stopa = stopaProcenat;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return stopa;
	}
	
	public static double getPrice(Integer prID) {
		double cena = 0;
		
		//PDV
		Proizvod proizvod = Proizvod.findById(prID);
//		GrupaProizvoda grupaProizvoda = proizvod.grupaProizvoda;
//		PDV pdv = grupaProizvoda.pdv;
		
		//CENA
		
		
		try {
			// StopaPDVa
//			PreparedStatement ps = DB.getConnection().prepareStatement(PDV_STATEMENT);
//			ps.setInt(1, pdv.id);
//			ResultSet resPDV = ps.executeQuery();
//			resPDV.next();
//			
//			Integer stopaID = resPDV.getInt(1);
//			Date stopaDatum = resPDV.getDate(2);
//			Double stopaProcenat = resPDV.getDouble(3);
			
			// Cenovnik
			PreparedStatement cs = DB.getConnection().prepareStatement(CENOVNIK_STATEMENT);
			ResultSet resCenovnik = cs.executeQuery();
			resCenovnik.next();
			
			Integer cenovnikID = resCenovnik.getInt(1);
			Date cenovnikDatum = resCenovnik.getDate(2);
			Cenovnik cenovnik = Cenovnik.findById(cenovnikID);
			
			StavkaCenovnika stavkaCenovnika = (StavkaCenovnika) StavkaCenovnika.find("byCenovnikAndProizvod", cenovnik, proizvod).fetch().get(0);
			cena = stavkaCenovnika.cena;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return cena;
	}
}
