package models.dokumenti;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.google.gson.annotations.Expose;

import models.akteri.Cenovnik;
import models.akteri.GrupaProizvoda;
import models.akteri.JedinicaMere;
import models.akteri.PDV;
import models.akteri.Proizvod;
import models.akteri.StavkaCenovnika;
import play.db.DB;
import play.db.jpa.GenericModel;

@Entity(name="StavkeFakture")
public class StavkaFakture extends GenericModel{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	@Expose
	public Integer id;
	
	@Column(length = 15)
	@Expose
	public Double jedCena;
	
	@Column(length = 10)
	@Expose
	public Double kolicina;
	
	@Column(length = 15, insertable = false)
	@Expose
	public Double vrednost;
	
	@Column(length = 2)
	@Expose
	public Double stopaRabata;
	
	@Column(length = 15, insertable = false)
	@Expose
	public Double iznosRabata;
	
	@Column(length = 15, insertable = false)
	@Expose
	public Double osnovica;
	
	@Column(length = 2)
	@Expose
	public Double stopaPDVa;
	
	@Column(length = 15, insertable = false)
	@Expose
	public Double iznosPDVa;
	
	@Column(length = 15, insertable = false)
	@Expose
	public Double ukupanIznos;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "faktura", referencedColumnName = "id", nullable = false)
	@Expose
	public Faktura faktura;
	
//	TODO: preuzeti iz aktera
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "proizvod", referencedColumnName = "id", nullable = false)
	@Expose
	public Proizvod proizvod;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "jedinicaMere", referencedColumnName = "id", nullable = false)
	@Expose
	public JedinicaMere jedinicaMere;

	public StavkaFakture() {
		super();
	}

	public StavkaFakture(JedinicaMere jedMere, Double jedCena, Double kolicina, Double stopaRabata,
			Double stopaPDVa, Faktura faktura, Proizvod proizvod) {
		super();
		this.jedinicaMere = jedMere;
		this.jedCena = jedCena;
		this.kolicina = kolicina;
		this.vrednost = this.jedCena*this.kolicina;
		this.stopaRabata = stopaRabata;
		this.iznosRabata = this.vrednost*this.stopaRabata;
		this.osnovica = this.vrednost-this.iznosRabata;
		this.stopaPDVa = stopaPDVa;
		this.iznosPDVa = this.osnovica*this.stopaPDVa;
		this.ukupanIznos = this.osnovica+this.iznosPDVa;
		this.faktura = faktura;
		this.proizvod = proizvod;
	}
	
	private static String PDV_STATEMENT = "SELECT id, max(datum), procenat, pdv FROM StopePDVa WHERE pdv=? GROUP BY id, procenat, pdv";
	private static String CENOVNIK_STATEMENT = "SELECT id, max(datumVazenja) FROM Cenovnik GROUP BY id";
	
	public static double getStopaPDVa(Integer prID){
		Double stopa = null;
		Proizvod proizvod = Proizvod.findById(prID);
		GrupaProizvoda grupaProizvoda = proizvod.grupaProizvoda;
		PDV pdv = grupaProizvoda.pdv;
		
		PreparedStatement ps;
		try {
			ps = DB.getConnection().prepareStatement(PDV_STATEMENT);
			ps.setInt(1, pdv.id);
			ResultSet resPDV = ps.executeQuery();
			resPDV.next();
			
			Integer stopaID = resPDV.getInt(1);
			Date stopaDatum = resPDV.getDate(2);
			Double stopaProcenat = resPDV.getDouble(3);
			stopa = stopaProcenat;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return stopa;
	}
	
	public static double getPrice(Integer prID) {
		double cena = 0;
		Proizvod proizvod = Proizvod.findById(prID);
		try {
			PreparedStatement cs = DB.getConnection().prepareStatement(CENOVNIK_STATEMENT);
			ResultSet resCenovnik = cs.executeQuery();
			resCenovnik.next();
			
			Integer cenovnikID = resCenovnik.getInt(1);
			Date cenovnikDatum = resCenovnik.getDate(2);
			Cenovnik cenovnik = Cenovnik.findById(cenovnikID);
			
			StavkaCenovnika stavkaCenovnika = (StavkaCenovnika) StavkaCenovnika.find("byCenovnikAndProizvod", cenovnik, proizvod).first();
			cena = stavkaCenovnika.cena;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return cena;
	}
	
}
