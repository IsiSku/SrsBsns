package models.dokumenti;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.google.gson.annotations.Expose;

import models.akteri.PoslovnaGodina;
import models.akteri.Preduzece;
import play.db.jpa.GenericModel;

/*
 * Otpremnica-racun
 */

@Entity(name="Fakture")
public class Faktura extends GenericModel {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	@Expose
	public Integer id;
	
	@Column(length=20, unique = true, nullable = false)
	@Expose
	public Integer broj;
	
	@Column(nullable = false)
	@Expose
	public Date datum;
	
	@Column(nullable = false)
	@Expose
	public Date datumValute;
	
	@Column(length=20, insertable = false)
	@Expose
	public Double ukupanRabat;
	
	@Column(length=20, insertable = false)
	@Expose
	public Double ukupanIznosBezPDVa;
	
	@Column(length=20, insertable = false)
	@Expose
	public Double ukupanIznosPDVa;
	
	@Column(length=20, insertable = false)
	@Expose
	public Double ukupanIznosZaPlacanje;
	
	@Column(length=1)
	@Expose
	public String smer;
	
	@Column(length=20)
	@Expose
	public String napomena;
	
	@Column(length=20)
	@Expose
	public String prevoznik;
	
	@Column(length=20)
	@Expose
	public String brVozila;
	
	@Column(length=20)
	@Expose
	public String vozac;
	
	@Column(length=20)
	@Expose
	public String odgovornoLice;
	
	@Column(length=20)
	@Expose
	public String primioRobu;
	
	@OneToMany(fetch=FetchType.LAZY, mappedBy="faktura" )
	public List<StavkaFakture> stavke = new ArrayList<StavkaFakture>();
	
//	@ManyToOne(fetch=FetchType.LAZY)
//	public OtpPri otpremnica;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "narudzbenica", referencedColumnName = "id", nullable = false)
	@Expose
	public Narudzbenica narudzbenica;
	
//	TODO: preuzeti iz aktera
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "godina", referencedColumnName = "id", nullable = false)
	@Expose
	public PoslovnaGodina poslovnaGodina;

//	TODO: preuzeti iz aktera
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "odKoga", referencedColumnName = "id", nullable = false)
	@Expose
	public Preduzece odKoga;
	
//	TODO: preuzeti iz aktera
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "zaKoga", referencedColumnName = "id", nullable = false)
	@Expose
	public Preduzece zaKoga;

	public Faktura() {
		super();
	}

	public Faktura(int broj, Date datum, Date datumValute, Double ukupanRabat, Double ukupanIznosBezPDVa,
			Double ukupanIznosPDVa, Double ukupanIznosZaPlacanje, String smer, String napomena, String prevoznik,
			String brVozila, String vozac, String odgovornoLice, String primioRobu, List<StavkaFakture> stavke,
			Narudzbenica narudzbenica, PoslovnaGodina godina, Preduzece odKoga, Preduzece zaKoga) {
		super();
		this.broj = broj;
		this.datum = datum;
		this.datumValute = datumValute;
		this.ukupanRabat = null;
		this.ukupanIznosBezPDVa = null;
		this.ukupanIznosPDVa = null;
		this.ukupanIznosZaPlacanje = null;
		for(StavkaFakture stavka: stavke){
			this.ukupanRabat+=stavka.iznosRabata;
			this.ukupanIznosBezPDVa+=stavka.osnovica;
			this.ukupanIznosPDVa+=stavka.iznosPDVa;
			this.ukupanIznosZaPlacanje+=stavka.ukupanIznos;
		}
		
		this.smer = smer;
		this.napomena = napomena;
		this.prevoznik = prevoznik;
		this.brVozila = brVozila;
		this.vozac = vozac;
		this.odgovornoLice = odgovornoLice;
		this.primioRobu = primioRobu;
		this.stavke = stavke;
		this.narudzbenica = narudzbenica;
		this.poslovnaGodina = godina;
		this.odKoga = odKoga;
		this.zaKoga = zaKoga;
	}
	
}
