package models.dokumenti;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.google.gson.annotations.Expose;

import models.akteri.PoslovnaGodina;
import models.akteri.Preduzece;
import play.db.jpa.GenericModel;
 
@Entity(name="Narudzbenice")
public class Narudzbenica extends GenericModel {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	@Expose
	public Integer id;
	
	@Column(length=20, unique = true, nullable = false)
	@Expose
	public Integer broj;
	
	@Column(nullable = false)
	@Expose
	public Date datum;
	
	@OneToMany(fetch=FetchType.LAZY, mappedBy="narudzbenica" )
	public List<StavkaNarudzbenice> stavke = new ArrayList<StavkaNarudzbenice>();
	
	@OneToMany(fetch=FetchType.LAZY, mappedBy="narudzbenica")
	public List<Faktura> fakture = new ArrayList<Faktura>();
	
	
//	TODO: preuzeti iz aktera
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "godina", referencedColumnName = "id", nullable = false)
	@Expose
	public PoslovnaGodina poslovnaGodina;

//	TODO: preuzeti iz aktera
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "odKoga", referencedColumnName = "id", nullable = false)
	@Expose
	public Preduzece odKoga;
	
//	TODO: preuzeti iz aktera
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "zaKoga", referencedColumnName = "id", nullable = false)
	@Expose
	public Preduzece zaKoga;

	public Narudzbenica() {
		super();
	}

	public Narudzbenica(int broj, Date datum, ArrayList<StavkaNarudzbenice> stavke, ArrayList<Faktura> fakture,
			PoslovnaGodina godina, Preduzece odKoga, Preduzece zaKoga) {
		super();
		this.broj = broj;
		this.datum = datum;
		this.stavke = stavke;
		this.fakture = fakture;
		this.poslovnaGodina = godina;
		this.odKoga = odKoga;
		this.zaKoga = zaKoga;
	}

	
	
}
