package models.akteri;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.google.gson.annotations.Expose;

import play.db.jpa.GenericModel;

/**
 * Created by Nemanja on 21/6/2016.
 */

@Entity
public class PoslovnaGodina extends GenericModel{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	@Expose
	public Integer id;

	@Column(length = 4, nullable = false)
	@Expose
	public Short godina;   

	@Column(nullable = true)
	@Expose
	public Boolean zakljucena;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "preduzece", referencedColumnName = "id", nullable = false)
	@Expose
	public Preduzece preduzece;

	public PoslovnaGodina(short godina, Boolean zakljucena) {
		super();

		this.godina = godina;
		this.zakljucena = zakljucena;
	}
	
	public PoslovnaGodina() {
		super();
	}
}
