package models.akteri;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.google.gson.annotations.Expose;

import play.db.jpa.GenericModel;

/**
 * Created by Nemanja on 21/6/2016.
 */
@Entity
public class JedinicaMere extends GenericModel {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	@Expose
	public Integer id;

	@Column(nullable = false, length = 100)
	@Expose
	public String naziv;

	@Column(nullable = false, length = 5, unique = true)
	@Expose
	public String skracenica;

	@OneToMany(mappedBy = "jedinicaMere")
	public List<Proizvod> proizvodList = new ArrayList<Proizvod>();
}
