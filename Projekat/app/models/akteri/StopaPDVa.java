package models.akteri;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.google.gson.annotations.Expose;

import play.db.jpa.GenericModel;

@Entity(name="StopePDVa")
public class StopaPDVa extends GenericModel{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	@Expose
	public Integer id;
	
	@Column(length = 5)
	@Expose
	public Double procenat;
	
	@Column(length = 10)
	@Expose
	public Date datum;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "pdv", referencedColumnName = "id", nullable = false)
	@Expose
	public PDV pdv;

	public StopaPDVa() {
		super();
	}
	
	
}
