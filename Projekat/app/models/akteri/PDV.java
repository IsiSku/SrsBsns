package models.akteri;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.google.gson.annotations.Expose;

import play.db.jpa.GenericModel;

@Entity(name="PDV")
public class PDV extends GenericModel{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	@Expose
	public Integer id;

	@Column(length = 120)
	@Expose
	public String naziv;
	
	@OneToMany(fetch=FetchType.LAZY, mappedBy="pdv")
	public List<GrupaProizvoda> grupe;
	
	@OneToMany(fetch=FetchType.LAZY, mappedBy="pdv")
	public List<StopaPDVa> stope = new ArrayList<StopaPDVa>();

	public PDV() {
		super();
	}
	
	
}
