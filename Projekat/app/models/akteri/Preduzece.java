package models.akteri;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.google.gson.annotations.Expose;

import play.db.jpa.GenericModel;

/**
 * Created by Nemanja on 21/6/2016.
 */
@Entity
public class Preduzece extends GenericModel {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	@Expose
	public Integer id;
	
	@Column(nullable = false, length = 150, unique = true)
	@Expose
	public String naziv;

	@Column(length = 10, nullable = false, unique = true)
	@Expose
	public String PIB;

	@Column(length = 8, nullable = false, unique = true)
	@Expose
	public String maticniBroj;

	@Column(length = 100)
	@Expose
	public String ulica;

	@Column(nullable = true)
	@Expose
	public Integer brojUlice;

	@Column(length = 10)
	@Expose
	public String brojTelefona;

	@Column(length = 50)
	@Expose
	public String email;

	@Column(length = 50)
	@Expose
	public String web;

	@Column(nullable = false)
	@Expose
	public Boolean nasePreduzece;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "preduzece")
	@Expose(serialize=false, deserialize=false)
	public List<Proizvod> proizvodList = new ArrayList<Proizvod>();

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "preduzece")
	@Expose(serialize=false, deserialize=false)
	public List<Cenovnik> cenovnikList = new ArrayList<Cenovnik>();

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "saradjujeSaPreduzecem")  //TODO Obrati pažnju na ovo
	@Expose(serialize=false, deserialize=false)
	public List<PoslovniPartner> ListaNasihPoslovnihPartnera = new ArrayList<PoslovniPartner>();

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "nasePreduzece")
	@Expose(serialize=false, deserialize=false)
	public List<PoslovniPartner> ListaKomeSmoMiPoslovniPartneri = new ArrayList<PoslovniPartner>();

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "mesto", referencedColumnName = "id", nullable = false)
	@Expose(serialize=true, deserialize=true)
	public Mesto mesto;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "preduzece")
	@Expose(serialize=false, deserialize=false)
	public List<PoslovnaGodina> poslovnaGodinaList = new ArrayList<PoslovnaGodina>();
}
