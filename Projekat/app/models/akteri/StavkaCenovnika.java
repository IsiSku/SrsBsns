package models.akteri;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.google.gson.annotations.Expose;

import play.db.jpa.GenericModel;

/**
 * Created by Nemanja on 21/6/2016.
 */
@Entity
public class StavkaCenovnika extends GenericModel {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	@Expose
	public Integer id;

	@Column(nullable = false, length = 100)
	@Expose
	public String naziv;

	@Column(nullable = false, length = 15, precision = 2)
	@Expose
	public Double cena;

	@ManyToOne
	@JoinColumn(name = "cenovnik", referencedColumnName = "id", nullable = false)
	@Expose
	public Cenovnik cenovnik;

	@ManyToOne
	@JoinColumn(name = "proizvod", referencedColumnName = "id", nullable = false)
	@Expose
	public Proizvod proizvod;
	
	public StavkaCenovnika() {
		
	}

	public StavkaCenovnika(String naziv, double cena, Cenovnik cenovnik, Proizvod proizvod) {
		super();
		this.naziv = naziv;
		this.cena = cena;
		this.cenovnik = cenovnik;
		this.proizvod = proizvod;
	}
}
