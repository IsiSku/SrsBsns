package models.akteri;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.google.gson.annotations.Expose;

import play.db.jpa.GenericModel;

/**
 * Created by Nemanja on 21/6/2016.
 */
@Entity
public class Proizvod extends GenericModel{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	@Expose
	public Integer id;

	@Column(nullable = false, length = 100)
	@Expose
	public String naziv;

	@Column(nullable = false, length = 1)
	@Expose
	public String tip;  //TODO realizovati kao enumerable


	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "jedinicaMere", referencedColumnName = "id", nullable = false)
	@Expose
	public JedinicaMere jedinicaMere;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "grupaProizvoda", referencedColumnName = "id", nullable = false)
	@Expose
	public GrupaProizvoda grupaProizvoda;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "preduzece", referencedColumnName = "id", nullable = false)
	@Expose
	public Preduzece preduzece;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "proizvod")
	public List<StavkaCenovnika> proizvodJeStavkaCenovnika = new ArrayList<StavkaCenovnika>();
}
