package models.akteri;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.google.gson.annotations.Expose;

import play.db.jpa.GenericModel;

/**
 * Created by Nemanja on 21/6/2016.
 */

@Entity
public class Mesto extends GenericModel {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	@Expose
	public Integer id;

	@Column(nullable = false, unique = true)
	@Expose
	public Integer postanskiBroj;

	@Column(nullable = false)
	@Expose
	public String naziv;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "mesto")
	public List<Preduzece> preduzeca = new ArrayList<Preduzece>();

//	public Mesto() {
//		super();
//	}
//	
//	public Mesto(int postanskiBroj, String naziv) {
//		super();
//
//		this.postanskiBroj = postanskiBroj;
//		this.naziv = naziv;
//	}
}
