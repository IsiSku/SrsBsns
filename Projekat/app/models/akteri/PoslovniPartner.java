package models.akteri;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.google.gson.annotations.Expose;

import play.db.jpa.GenericModel;

/**
 * Created by Nemanja on 21/6/2016.
 */
@Entity
public class PoslovniPartner extends GenericModel {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	@Expose
	public Integer id;
	
	@Column(nullable = false, length = 1)
	@Expose
	public String tipSaradnje;  //TODO ovo je enumeracija. Treba je tako i implementirati

	@Expose
	public Date datumPocetka;

	@Expose
	public Date datumZavrsetka;

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "nasePreduzece", referencedColumnName = "id", nullable = false)
	@Expose
	public Preduzece nasePreduzece;

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "saradjujeSaPreduzecem", referencedColumnName = "id", nullable = false)
	@Expose
	public Preduzece saradjujeSaPreduzecem;

	public PoslovniPartner(String tipSaradnje, Date datumPocetka, Date datumZavrsetka, Preduzece nasePreduzece, Preduzece saradjujeSaPreduzecem) {
		super();

		this.tipSaradnje = tipSaradnje;
		this.datumPocetka = datumPocetka;
		this.datumZavrsetka = datumZavrsetka;
		this.nasePreduzece = nasePreduzece;
		this.saradjujeSaPreduzecem = saradjujeSaPreduzecem;
	}
}
