package models.akteri;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.google.gson.annotations.Expose;

import play.db.jpa.GenericModel;
import play.db.jpa.Model;

/**
 * Created by Nemanja on 21/6/2016.
 */
@Entity
public class Cenovnik extends GenericModel{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	@Expose
	public Integer id;

	@Column(nullable = false)
	@Expose
	public Date datumVazenja;

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "preduzece", referencedColumnName = "id", nullable = false)
	@Expose
	public Preduzece preduzece;

	@OneToMany(mappedBy = "cenovnik")
	public List<StavkaCenovnika> stavkeCenovnika;
}
