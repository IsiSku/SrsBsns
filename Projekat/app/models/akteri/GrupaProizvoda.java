package models.akteri;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.google.gson.annotations.Expose;

import play.db.jpa.GenericModel;

@Entity(name="GrupeProizvoda")
public class GrupaProizvoda extends GenericModel{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	@Expose
	public Integer id;

	@Column(length = 20, unique = true, nullable = false)
	@Expose
	public String naziv;
	
	@OneToMany(fetch=FetchType.LAZY, mappedBy="grupaProizvoda")
	public List<Proizvod> proizvodi = new ArrayList<Proizvod>();
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "pdv", referencedColumnName = "id", nullable = false)
	@Expose
	public PDV pdv;

	public GrupaProizvoda() {
		super();
	}
	
	
}
