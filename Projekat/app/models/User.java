package models;

import play.db.jpa.Model;

import javax.persistence.Column;
import javax.persistence.Entity;

/**
 * Created by Nemanja on 22/6/2016.
 */
@Entity(name = "Users")
public class User extends Model {

	@Column(unique = true, nullable = false)
	public String username;

	@Column(nullable = false)
	public String password;

	public User(String username, String password) {
		super();

		this.username = username;
		this.password = password;
	}
}
