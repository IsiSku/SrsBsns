"use strict";

(function() {
	var app = angular.module('app');
	
	app.controller('preduzeceCtrl', function($scope, $http, $mdToast) {
		$scope.init = function() {
			$http.get('/pred/get')
				.then(
					function(response) {
						$scope.company = response.data[0];
					},
					function(reason) {
						$mdToast.show({
							template: '<md-toast>Greška pri dobavljanju podataka</md-toast>',
							hideDelay: 3000,
							position: 'top right',
							parent: '#toastParent'
						});
					}
				);
		};
		$scope.init();
		
		$scope.showEditForm = false;
		$scope.showEditBtn = true;
		
		$scope.editInfo = function() {
			$scope.showEditForm = true;
			$scope.showEditBtn = false;
		};
		
		$scope.cancel = function() {
			$scope.showEditForm = false;
			$scope.showEditBtn = true;
		};
		
		$scope.saveInfo = function() {
			$http.post('/pred/upd') //potrebno je da backend vrati podatke u obliku kao sto je $scope.company
				.then(
						function(response) {
							$scope.company = response.data;
						},
						function(reason) {
							$mdToast.show({
								template: '<md-toast>Greška pri izmeni podataka</md-toast>',
								hideDelay: 3000,
								position: 'top right',
								parent: '#toastParent'
							});
						}
				);
		};
	});
}());