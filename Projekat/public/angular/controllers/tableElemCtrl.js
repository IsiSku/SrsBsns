"use strict";

(function() {
	var app = angular.module('app');
	
	app.controller('tableElemCtrl', function($scope, $http, $window, tableService, row, $mdToast) {
		$scope.boolVals = [true, false];
				
		$scope.addFormSubmit = function() {
			
			$mdToast.hide();
		}
	});
}());