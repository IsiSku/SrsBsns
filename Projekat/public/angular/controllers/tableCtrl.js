"use strict";

(function() {
	var app = angular.module('app');
	
	app.controller('tableCtrl', function($scope, $q, $http, $window, $mdToast, tableService, nextService, zoomService) {
		if(tableService.selectedTable === '' || tableService.tableName === '') {
			$window.location.href = '#/';
			return;
		}
		
		$scope.tableSettings = {
				order: 'name',
				limit: 25,
				page: 1
		};
		$scope.selected = [];
		$scope.order = 'nameToLower';
		
		$scope.boolVals = [true, false];
		$scope.newRowData = [];		
		$scope.templateRow = [];
		
		$scope.cards = ["angular/includes/table.html", "angular/includes/toastForm.html"];
		$scope.formMode = 'edit';
		$scope.nextTables = [];
		$scope.next = nextService.isNext;
		$scope.lookup = false;
		
		$scope.tableData = {};
		$scope.tableData.rows = [];
		$scope.theads = [];
		$scope.tableData.title = tableService.tableName;

		$scope.init = function(nextTableModelName, nextTableViewName) {
			$scope.nextTableName = nextTableModelName;
			$scope.tableData.title = nextTableViewName;
			$scope.getData(undefined, nextService.id);
		};
		
		$scope.createFaktura = function (){
//			var temp = [];
//			$scope.tableData.rows.forEach(function(element){
//				var temp1 = {};
//				element.forEach(function(elem){
//					temp1[elem.name] = elem.val;
//				});
//				temp.push(temp1);
//			});
			$http.post('/faktura/' + $scope.selected[0][0].val).then(
					function(response){
						alert('Uspesno');
					},
					function(reason) {
						$mdToast.show({
							template: '<md-toast>Greška pri formiranju Fakture</md-toast>',
							hideDelay: 3000,
							position: 'top right',
							parent: '#toastParent'
						});
//						console.log(reason);
//						
//						deferred.resolve($scope);
					});
		}
		
		$scope.getNextData = nextService.getNextData;
		$scope.getData = function(table, id) {
			if($scope.next && (!$scope.nextTableName || $scope.nextTableName === '')) {
				return;
			}
			
			var fromTable = '';
			fromTable = tableService.selectedTable;
			// ako smo u next-u, da uzmemo podatke za next tabelu
			if($scope.nextTableName && $scope.nextTableName !== '') {
				fromTable = $scope.nextTableName;
			}
			// svejedno da li je next ili ne, ako smo zoom, uzmi zoom tabelu
			// zoom ce morati da se napravi kao niz tabela u redosledu kojim su otvarane
			if(table) {
				fromTable = table;
			}
			var deferred = $q.defer();
			var promise = !id ? $http.get('table/' + fromTable + '/data') : $http.get('table/' + fromTable + '/data/' + tableService.selectedTable + '/' + id);
			promise.then(
					function(response) {
						// response.data[0] redovi tabele, response.data[1] naziv polja za lookup (nije 'id')
						// response.data[2] tipovi podataka, response.data[3] da li se polje prikazuje
						response.data[0].forEach(function(current) {
							var q = {};
							var row = [];									
							for(q in current) {
								var qVal = current[q];
								var qDisplay = true;
								if(response.data[3]) {
									qDisplay = response.data[3][q] === false ? response.data[3][q] : true;
								}
								
								if(Object.prototype.toString.call(qVal) === '[object Object]') {
									var qOtherField = response.data[1][q];
									var qOtherVal = qVal[qOtherField]
									var qOtherType = response.data[2][q];
									row.push({val:qVal['id'], name:q, type:'number', foreign:true, otherVal:qOtherVal,
										otherValField:qOtherField, otherValType:qOtherType, display: qDisplay});
								}
								else {
									var qType = response.data[2][q];
									row.push({val:qVal, name:q, type:qType, foreign:false, display: qDisplay});
								}
							}
							
							if(row[0].val) { // ako nismo dobili podatke o praznom redu (tj da tabela nije prazna)
								$scope.formMode = 'edit';
								$scope.tableData.rows.push(row);
							}
							else {
								$scope.setTemplateRow(row);
							}
						});

						if(!$scope.next) { //next nije omogucen u next-u
							$scope.getNextData()
								.then(
										function(result) {
											$scope.nextTables = result;
										},
										function(fail) {
											// ERROR MdToastERROR MdToastERROR MdToastERROR MdToast
										}
								);
						}
						
						if($scope.tableData.rows.length < 1) {
							if(!$scope.next) {
								$mdToast.show({
									template: '<md-toast>Tabela ne sadrži podatke</md-toast>',
									hideDelay: 3000,
									position: 'top right',
									parent: '#toastParent'
								});
							}
							
							$scope.theads = [];
							$scope.templateRow.forEach(function(cRow){
								$scope.theads.push({name:cRow.name, type:cRow.type});
								if(cRow.foreign) {
									$scope.theads.push({name:cRow.name + ' - ' + cRow.otherValField, type:cRow.otherValType});
								}
							});
							$scope.formatNewRow($scope.templateRow);
							$scope.formMode = 'add';
							deferred.resolve($scope);
							return;
						}
						
//						$scope.templateRow = [];
						$scope.theads = [];
						$scope.tableData.rows[0].forEach(function(cRow){
							$scope.theads.push({name:cRow.name, type:cRow.type});
							if(cRow.foreign) {
								$scope.theads.push({name:cRow.name + ' - ' + cRow.otherValField, type:cRow.otherValType});
							}
							$scope.templateRow.push({val:null, name:cRow.name, type:cRow.type, foreign:cRow.foreign,
								otherValField:cRow.otherValField, otherVal:null, otherValType:cRow.otherValType, display: cRow.display});
						});
//						$scope.newRow = $scope.templateRow;
						$scope.formatNewRow($scope.tableData.rows[0]);
						$scope.tableFs.setSelected();
						
						deferred.resolve($scope);
					},
					function(reason) {
						$mdToast.show({
							template: '<md-toast>Greška pri preuzimanju podataka</md-toast>',
							hideDelay: 3000,
							position: 'top right',
							parent: '#toastParent'
						});
						console.log(reason);
						
						deferred.resolve($scope);
					}
			);
			return deferred.promise;
		};
		
		$scope.tableFs = {
				faktura: function() {
					$http({
						method: 'POST',
						url: '/report/faktura/pdf',
						params: {
							faktura: getId()
						},
						headers: {
							accept: 'application/pdf'
						},
						responseType: 'arraybuffer'
					}).then(
						function(response) {
							var blob = new Blob([response], { type: 'application/pdf' });
							//var url = $window.URL || $window.webkitURL;
    						var fileUrl = URL.createObjectURL(blob);
    						$window.open(fileUrl);
						},
						function(reason) {
							$mdToast.show({
								template: '<md-toast>Greška pri generisanju PDF-a</md-toast>',
								hideDelay: 3000,
								position: 'top right',
								parent: '#toastParent'
							});
							console.log(reason);
						}
					);
				},
				dodaj: function() {
					$scope.formatNewRow(undefined, true);
					$scope.setTemplateRow($scope.newRow);
					$scope.formatNewRow($scope.templateRow);
					$scope.selected = [];
					$scope.formMode = 'add';
				},
				ukloni: function() {
					$scope.formMode = 'delete';
					
					var id = getId();
					if(id === -1) {
						$scope.tableFs.setSelected();
						return;
					}
					$http.delete('/table/' + tableService.selectedTable + '/del/' + id)
						.then(
								function(response) {
									var idx = $scope.tableData.rows.indexOf($scope.selected[0]);
									if(idx != -1) {
										$scope.tableData.rows.splice(idx, 1);
									}
									
									$scope.selected = [];
									$scope.tableFs.setSelected();
									$scope.formMode = 'edit';
								},
								function(reason) {
									$mdToast.show({
										template: '<md-toast>Greška pri uklanjanju: ' + reason.data + '</md-toast>',
										hideDelay: 3000,
										position: 'top right',
										parent: '#toastParent'
									});
									
									$scope.formMode = 'edit';
								}
						);
				},
				izmeni: function() {
//					$scope.newRow = $scope.selected[0];
					$scope.formatNewRow($scope.selected[0]);
					
//					$scope.cards.unshift("angular/includes/toastForm.html");
					$scope.formMode = 'edit';
				},
				search: function() {
					$scope.formMode = 'search';
				},
				next: function() {
					nextService.link = tableService.link;
					nextService.isNext = true;
					nextService.id = getId();
					if(nextService.id === -1) {
						$scope.tableFs.setSelected();
						return;
					}
					
					$window.location.href = "#/table/next";
				},
				nextReturn: function() {
					nextService.reset();
					$window.location.href = nextService.link;
					
					
				},
				zoom: function(otherValSrc, otherValField) {
					// zoom ce morati da se napravi kao niz tabela u redosledu kojim su otvarane
					zoomService.parentTable = tableService.selectedTable;
					zoomService.selectedIdx = $scope.tableData.rows.indexOf($scope.selected[0]);
					$scope.selected = [];
					zoomService.oldFormMode = $scope.formMode;

					var tables = tableService.tables;

					zoomService.otherValSrc = otherValSrc;
					if(otherValSrc === 'odKoga' || otherValSrc === 'zaKoga' || otherValSrc === 'preduzece') {
						otherValSrc = 'PoslovniPartner';
					}
					
					$scope.formatNewRow(undefined, false, true);
					zoomService.parentTableData = cloneObject($scope.tableData);
					zoomService.rowData = cloneObject($scope.newRow);
					zoomService.otherValField = otherValField;
					zoomService.theads = $scope.theads;

					$scope.tableData = {};
					$scope.tableData.rows = [];
					var lookupTable = tables.acters.find(function(elem) {
						return elem.m.toLowerCase() === otherValSrc.toLowerCase();
					});
					if(lookupTable) {
						$scope.tableData.title = lookupTable.v;
					}
					
					if(!lookupTable) {
						lookupTable = tables.docs.find(function(elem) {
							return elem.m.toLowerCase() === otherValSrc.toLowerCase();
						});
						if(lookupTable) {
							$scope.tableData.title = lookupTable.v;
						}
						
						if(!lookupTable) {
							lookupTable = tables.special.find(function(elem) {
								return elem.m.toLowerCase() === otherValSrc.toLowerCase();
							});
							if(lookupTable) {
								$scope.tableData.title = lookupTable.v;
							}
							else {
								$mdToast.show({
									template: '<md-toast>Nije moguće naći traženu tabelu</md-toast>',
									hideDelay: 3000,
									position: 'top right',
									parent: '#toastParent'
								});
								zoomService.reset();
								return;
							}
						}
					}
					$scope.lookup = true;
					
					if(lookupTable && zoomService.parentTableData) {
						tableService.selectedTable = lookupTable.m;
						$scope.getData(lookupTable.m)
							.then(function(result) {
								result.tableFs.setSelected(); // ne radi zbog lookup uslova u toj funkciji
							});
					}
					
					// ERROR MdToast ERROR MdToast ERROR MdToast ERROR MdToast ERROR MdToast ERROR MdToast ERROR MdToast ERROR MdToast
				},
				zoomReturn: function() {
					// zoom ce morati da se napravi kao niz tabela u redosledu kojim su otvarane
					var index = zoomService.selectedIdx;
					zoomService.selectedRow = cloneObject($scope.selected[0]);
					$scope.theads = zoomService.theads;

					tableService.selectedTable = zoomService.parentTable;
					$scope.tableData.rows = [];
					$scope.tableData.title = zoomService.parentTableData.title;
					$scope.selected = [];
					
					$scope.getData()
						.then(function(result) {
							$scope.newRow = cloneObject(zoomService.rowData);
							$scope.newRow.forEach(function(elem) {
								if(elem.foreign && elem.name === zoomService.otherValSrc) {
									var idVal = zoomService.selectedRow.find(function(vElem) {
										return vElem.name === 'id';
									});
									elem.val = idVal.val;
									 
									var otherVal = zoomService.selectedRow.find(function(vElem) {
										return vElem.name === zoomService.otherValField;
									});
									elem.otherVal = otherVal.val;
									
									return;
								}
							});
							$scope.formatNewRow($scope.newRow);
							
							$scope.formMode = zoomService.oldFormMode;
							if(index >= 0) {
								$scope.selected[0] = $scope.newRow;
							}
							else {
								$scope.selected = [];
							}
							
							zoomService.reset();
							$scope.lookup = false;
						});
				},
				setSelected: function(toSelect) {	
					if($scope.lookup) {
						return;
					}
					
					if($scope.formMode === 'add') {
						$scope.formatNewRow();
						$scope.formatNewRow($scope.newRow);
						return;
					}
					
					if(!$scope.tableData.rows[0]) {
						$scope.selected = [];
						return;
					}
					
					if($scope.selected.length !== 0 ) {
						$scope.formatNewRow($scope.selected[0]);
						return;
					}
					
					if(toSelect) {
						$scope.selected = [toSelect];
						$scope.formatNewRow($scope.selected[0]);
						return;
					}
					
					$scope.selected = [$scope.tableData.rows[0]];
					$scope.formatNewRow($scope.selected[0]);
				},
				disableSaveBtn: function() {
					return ($scope.selected.length < 1) && ($scope.formMode !== 'add');
				},
				disableDel: function() {
					return ($scope.selected.length < 1) || ($scope.tableData.rows.length < 1) || ($scope.formMode === 'delete');
				},
				disableAdd: function() {
					return $scope.formMode === 'add'; 
				},
				disableSearch: function() {
					return $scope.formMode === 'search'; 
				},
				disableEdit: function() {
					return ($scope.formMode === 'edit') || ($scope.tableData.rows.length < 1) || ($scope.selected.length < 1); 
				},
				disableNext: function() {
					return ($scope.tableData.rows.length < 1) || ($scope.selected.length < 1);
				},
				disableFaktura: function() {
					return ($scope.selected.length !== 1) || ($scope.tableData.rows.length < 1);
				}
		};

		$scope.toastFormSubmit = function() {
			var fromTable = '';
			fromTable = tableService.selectedTable;
			// ako smo u next-u, ali ne u next->zoom, da menjamo podatke za next tabelu
			if($scope.next && !$scope.zoom && $scope.nextTableName && $scope.nextTableName !== '') {
				fromTable = $scope.nextTableName;
			}
			
			if($scope.formMode == 'add') {
				$scope.formatNewRow(); // unwrap iz newRowData u newRow
				var data = prepareData($scope.newRow);
				$http.post('/table/' + fromTable + '/add', data)
					.then(
							function(response) {
								$mdToast.show({
									template: '<md-toast>Podaci uspešno dodati</md-toast>',
									hideDelay: 3000,
									position: 'top right',
									parent: '#toastParent'
								});
								var q = {};
								var row = [];
								for(q in response.data[0]) {
									var qVal = response.data[0][q];
									if(Object.prototype.toString.call(qVal) === '[object Object]') {
										var qType = typeof(qVal['id']);
										var qOtherVal = qVal[response.data[1][q]];
										row.push({val:qVal['id'], name:q, type:qType, foreign:true,
														otherVal:qOtherVal, otherValType:typeof(qOtherVal)});
									}
									else {
										var qType = typeof(qVal);
										row.push({val:qVal, name:q, type:qType, foreign:false});
									}
								}
								$scope.tableData.rows.push(row);

								$scope.setTemplateRow();
								$scope.formatNewRow($scope.templateRow);
								$scope.formatNewRow();
								$scope.tableFs.setSelected(row);
							},
							function(reason) {
								$mdToast.show({
									template: '<md-toast>Greška pri dodavanju podataka</md-toast>',
									hideDelay: 3000,
									position: 'top right',
									parent: '#toastParent'
								});
								
								$scope.tableFs.setSelected();
//								$scope.setTemplateRow();
							}
					);
			}
			else if($scope.formMode == 'edit') {
				$scope.formatNewRow();
				var rowIdx = $scope.tableData.rows.indexOf($scope.newRow);
				var data = prepareData($scope.newRow);
				$http.put('/table/' + fromTable + '/upd', data)
					.then(
							function(response) {
//								tableService.reload(); //PRI UPDATE+ZOOM, PODACI SE NE PROMENE U TABELI
								$mdToast.show({
									template: '<md-toast>Podaci uspešno izmenjeni</md-toast>',
									hideDelay: 3000,
									position: 'top right',
									parent: '#toastParent'
								});
							},
							function(reason) {
								$mdToast.show({
									template: '<md-toast>Greška pri izmeni podataka</md-toast>',
									hideDelay: 3000,
									position: 'top right',
									parent: '#toastParent'
								});

								$scope.getData();
							}
					);
			}
		};		
		var prepareData = function(newRow) {
			var data = {};
			var skip = 0;
			$scope.theads.forEach(function(head, idx) {
				if(head.name.indexOf('-') === -1) {
					var col = newRow[idx-skip];
					if(col.foreign) {
						data[head.name] = {id:col['val']};
						data[head.name][col.otherValField] = col.otherVal; 
					}
					else {
						data[head.name] = col['val'];
					}
				}else {
					skip+=1;
				}
			});
			
			return data;
		};
		$scope.formatNewRow = function(rowData, empty, clone) {	// newRow = rowData = [{}, {}, {}, ...]
			if(!rowData) {
				$scope.newRow = [];
				var data = $scope.newRowData;
				if(empty || clone) {
					data = cloneObject($scope.newRowData);
				}
				data.forEach(function(elem) {
					elem.forEach(function(elem) {
						if(empty) {
							elem.val = undefined;
							if(elem.otherVal) {
								elem.otherVal = undefined;
							}
						}
						$scope.newRow.push(elem);
					});
				});
			}
			else {
				var doIt = false;
				if($scope.newRowData && $scope.newRowData.length > 0) {
					if(!$scope.lookup) {
						$scope.formatNewRow();
					}
					doIt = rowData.some(function(elem) {
						return $scope.newRow.indexOf(elem) === -1;
					});
					if(!doIt && !$scope.lookup) {
						return;
					}
				}
				
				$scope.newRowData = [];					// newRowData = [[{}, {}, {}, ...], [{}, {}, {}, ...], ...]
				if(rowData.length < 5) {
					$scope.newRowData = [rowData];
//					$scope.formatNewRow();
					return;
				}
				
				var rowDataRow = 0;
				$scope.newRowData[rowDataRow] = [];
				rowData.forEach(function(elem, index) {
					if(index >= $scope.newRowData.length * 7) {
						rowDataRow++;
						$scope.newRowData[rowDataRow] = [];
					}
					
//					$scope.newRowData[rowDataRow].push(rowData[index]);
					$scope.newRowData[rowDataRow].push(elem);
				});
//				$scope.formatNewRow();
			}
		}
		$scope.setTemplateRow = function(templateRow) {
			if(!templateRow) {
				$scope.templateRow = [];
				$scope.tableData.rows[0].forEach(function(col){
					$scope.templateRow.push({val:null, name:col.name, type:col.type, foreign:col.foreign, display: col.display,
						otherValField:col.otherValField, otherVal:null, otherValType:col.otherValType});
				});
			}
			else {
				$scope.templateRow = [];
				templateRow.forEach(function(col){
					$scope.templateRow.push({val:col.val, name:col.name, type:col.type, foreign:col.foreign, display: col.display,
						otherValField:col.otherValField, otherVal:col.otherVal, otherValType:col.otherValType});
				});
			}
			
			return $scope;
		};
		var getId = function() {
			var selectedRow = $scope.selected[0];
			var id = -1;
			selectedRow.forEach(function(prop) {
				if(prop.name === 'id') {
					id = prop.val;
				}
			});			
			return id;
		}
		var cloneObject = function(obj) {
			if(Object.prototype.toString.call(obj) === '[object Array]') {
				var clone = [];
				if(obj.length > 0) {
					obj.forEach(function(current, idx) {
						if(Object.prototype.toString.call(current) === '[object Array]') {
							clone[idx] = cloneObject(current);
						}
						else if(Object.prototype.toString.call(current) === '[object Object]') {
							clone[idx] = cloneObject(current);
						}
						else {
							clone[idx] = current;
						}
					});
				}
				
				return clone;
			}
			else {
				var clone = {};
				for(var q in obj) {
					var qVal = obj[q];
					if(Object.prototype.toString.call(qVal) === '[object Array]') {
						clone[q] = cloneObject(qVal);
					}
					else if(Object.prototype.toString.call(qVal) === '[object Object]') {
						clone[q] = cloneObject(qVal);
					}
					else {
						clone[q] = obj[q];
					}
				}
				
				return clone;
			}
		};

		$scope.getData();
	});
}());