"use strict";

(function() {
	var app = angular.module('app');
	
	app.controller('nextCtrl', function($scope, $http, $window, $mdToast, tableService, nextService) {
		if(tableService.selectedTable === '' || tableService.tableName === '') {
			$window.location.href = '#/';
			return;
		}
		
		this.tableTabs = nextService.nextTables;
	});
}());