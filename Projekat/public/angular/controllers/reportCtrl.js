"use strict";

(function() {
	var app = angular.module('app');
	
	app.controller('reportCtrl', function($scope, reportService) {
		$scope.setData = function(data) {
	        $scope.xml = data;
	    };
	         
		reportService.get($scope.setData);
	});
}());