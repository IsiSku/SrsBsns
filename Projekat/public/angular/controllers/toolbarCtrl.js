"use strict";

(function() {
	var app = angular.module('app');
	
	app.controller('toolbarCtrl', function($scope, $http, tableService, nextService) {
		$scope.tables = tableService.tables;
		
		$scope.tableClick = function(table, tableName, special) {
			nextService.reset();
			tableService.tableClick(table, tableName, special);
		}
		$scope.testDb = function() {
			$http.get('/testDb')
				.then(
						function(response) {
							alert("OK");
						},
						function(reason) {
							alert("FAIL \n" + JSON.stringify(reason));
							console.error("FAIL \n" + JSON.stringify(reason));
						}
				);
		};
		
		$scope.openMenu = function($mdOpenMenu, ev) {
			$mdOpenMenu(ev);
		};
	});
}());