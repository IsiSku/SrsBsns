"use strict";

(function() {
	var app = angular.module('app');
	
	app.service('nextService', function($http, $window, $q, tableService) {
		var self = this;
		
		self.link = "";
		self.isNext = false;
		self.nextTables = [];
		self.id = -1;
		
		self.reset = function() {
			self.isNext = false;
			self.nextTables = [];
			self.id = -1;
		}
		
		self.getNextData = function() {
			var deferred = $q.defer();
			$http.get('/table/' + tableService.selectedTable + '/next')
			.then(
					function(response) {		
						self.nextTables = [];				
						response.data.forEach(function(elem) {
							var table = tableService.tables.acters.find(function(el) {
								return el.m === elem;
							});
							
							if(table && !self.nextTables.includes(table)) {
								self.nextTables.push(table);
							}
							else {
								table = tableService.tables.docs.find(function(el) {
									return el.m === elem;
								});
								
								if(table && !self.nextTables.includes(table)) {
									self.nextTables.push(table);
								}
							}
						});
						
						deferred.resolve(self.nextTables);
					},
					function(reason) {

						deferred.reject('FAILED');
					}
			);
			
			return deferred.promise;
		};
	});
}());