"use strict";

(function() {
	var app = angular.module('app');
	
	app.service('tableService', function($window, zoomService) {
		var self = this;
		
		self.link = '';
		self.tables = [];
		self.tables.acters = [{m:'Cenovnik', v:'Cenovnici', s:true}, {m:'GrupaProizvoda', v:'Grupe Proizvoda'},
				               {m:'JedinicaMere', v:'Jedinice Mere'}, {m:'Mesto', v:'Naseljena Mesta'}, {m:'PDV', v:'PDV'},
				               {m:'PoslovnaGodina', v:'Poslovne Godine'}, {m:'PoslovniPartner', v:'Poslovni Partneri'},
				               {m:'PoslovniPartner', v:'Preduzece'}, {m:'Proizvod', v:'Proizvodi'}, {m:'StopaPDVa', v:'Stope PDV-a'}];
		self.tables.docs = [{m:'Faktura', v:'Fakture', s:true}, {m:'Narudzbenica', v:'Narudžbenice', s:true}];
		self.tables.special = [{m:'StavkaCenovnika', v:'Stavke Cenovnika'}, {m:'StavkaFakture', v:'Stavke Fakture'},
		                       {m:'StavkaNarudzbenice', v:'Stavke Narudžbenice'}];
		
		self.selectedTable = '';
		self.tableName = '';
		self.sTable = '';
		self.sTableName = '';
		
		self.tableClick = function(table, tableName, special) {
			self.link = '#/table';
			self.selectedTable = table;
			self.tableName = tableName;
			if(special) {
				var cutName = table.substring(0, table.length - 1);
				var sTable = self.tables.special.find(function(elem) {
					return elem.m.indexOf(cutName) !== -1;
				});
				
				self.sTable = sTable.m;
				self.sTableName = sTable.v;
				self.link = self.link + '/s';
			}
			zoomService.reset();
			
			if($window.location.href.indexOf('#/table') != -1){
				// Svaka tabela ima isti url ('.../table') i zato moramo uraditi 'reload' stranice
				// Radi se preko servisa, jer je on stalno 'prisutan', a kontroler se 'pali' samo kad smo na njegovoj stranici
				$window.location.href = '#/';
				$window.location.href = self.link;
				return;
			}
			$window.location.href = self.link;
		};
		
		self.reload = function() {
			$window.location.href = '#/';
			$window.location.href = '#/table';
		};
	});
}());