"use strict";

(function() {
	var app = angular.module('app');
	
	app.service('zoomService', function() {
		var self = this;
		
		self.reset = function() {
			self.parentTable = undefined;
			self.parentTableData = undefined;
			self.rowData = undefined;
			self.selectedRow = undefined;
			self.otherValSrc = undefined;
			self.otherValField = undefined;
			self.oldFormMode = undefined;
			self.selectedIdx = undefined;
			self.theads = undefined;
		};
		
		self.reset();
	});
}());