"use strict";

(function() {
	var app = angular.module('app');
	
	app.directive('tableHeaders', function() {
		return {
			restrict: 'A',
			transclude: true,
			templateUrl: 'angular/templates/tableHeaders.html'
		};
	});
}());