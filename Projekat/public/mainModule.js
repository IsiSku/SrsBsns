"use strict";

(function() {
	var app = angular.module('app', ['ngMaterial', 'ngMessages', 'ngRoute', 'ngCookies', 'md.data.table']);
	
	app
	.config(function($routeProvider, $httpProvider){ //, $locationProvider
		$routeProvider
		.when('/', {
			templateUrl: 'angular/routes/preduzece.html',
			controller: 'preduzeceCtrl'
		})
		.when('/index', {
			templateUrl: 'angular/routes/preduzece.html',
			controller: 'preduzeceCtrl'
		})
		.when('/home', {
			templateUrl: 'angular/routes/preduzece.html',
			controller: 'preduzeceCtrl'
		})
		.when('/table', {
			templateUrl: 'angular/routes/tableDisplay.html',
			controller: 'tableCtrl'
		})
		.when('/table/s', {
			templateUrl: 'angular/routes/tableDisplayS.html'
		})
		.when('/table/next', {
			templateUrl: 'angular/routes/nextDisplay.html',
			controller: 'nextCtrl as next'
		})
		.when('/report', {
			templateUrl: 'angular/routes/report.html',
			controller: 'reportCtrl'
		})
		.otherwise({
			redirectTo:"/"
		});
	});
}());